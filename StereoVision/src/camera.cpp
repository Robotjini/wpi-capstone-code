#include <camera.h>
#define AUTO_WHITE_BALANCE_REG	0x000000C1
#define AUTO_WHITE_BALANCE		1
#define WHITE_BALANCE_OFFSET	0x80C
#define AUTO_BRIGHTNESS_REG		0x00000080
#define AUTO_BRIGHTNESS			1
#define BRIGHTNESS_OFFSET		0x800
#define AUTO_EXPOSURE_REG		0x00000080
#define AUTO_EXPOSURE			1
#define AUTO_EXPOSURE_OFFSET	0x804
/**
 * @brief Initializes the cameras, frames and size to null. It also initializes the context and the mutex lock
 */
//using namespace MagickCore;
Camera::Camera()
{
	cameras[0] = NULL;
	cameras[1] = NULL;
	leftframe = NULL;
	rightframe = NULL;
	height=0;
	width=0;
	dc_type = dc1394_new();
	pthread_mutex_init(&captureMutex,NULL);

}
/**
 * @brief Calls clean, and destroys the mutex
 */
Camera::~Camera()
{
	pthread_mutex_destroy(&captureMutex);
	clean();
}
/**
 * @brief Initializes both cameras with maximum size, framerate and speed
 */
void Camera::initialize(int _width,int _height)
{
	dc1394video_mode_t video_mode = DC1394_VIDEO_MODE_1024x768_RGB8;
	dc1394framerate_t framerate = DC1394_FRAMERATE_7_5;
	dc1394speed_t iso_speed = DC1394_ISO_SPEED_400;
	dc1394operation_mode_t opt_mode = DC1394_OPERATION_MODE_1394B;
	uint32_t white_reg=0;
	dc1394video_frame_t *frame=NULL;
	dc1394error_t err;

	height = _height;
	width = _width;

	if (dc1394_camera_enumerate(dc_type,&list) != DC1394_SUCCESS){
		fprintf( stderr, "Unable to look for cameras\n\n"
				"Please check \n"
				"  - if the kernel modules `ieee1394',`raw1394' and `ohci1394' are loaded \n"
				"  - if you have read/write access to /dev/raw1394\n\n");
		dc1394_free(dc_type);
		exit(1);
	}
	cout<<"number of cameras="<<list->num<<endl;
	for (int i=0;i<list->num;i++){
		cout<<"camera"<<i<<endl;
		cameras[i] = dc1394_camera_new(dc_type,list->ids[i].guid);



		if (_1394b){
			//set mode and speed to 1394B
			err = dc1394_video_set_operation_mode(cameras[i], DC1394_OPERATION_MODE_1394B);
			clean_and_exit(err,"Error setting operation mode");

			err = dc1394_video_set_iso_speed(cameras[i], DC1394_ISO_SPEED_800);
			clean_and_exit(err,"Error setting iso speed");

		}else{//else set to 400Mbits/sec
			err = dc1394_video_set_iso_speed(cameras[i], iso_speed);
			clean_and_exit(err,"Error setting iso speed1");
		}

		err = dc1394_video_set_mode(cameras[i], video_mode);
		clean_and_exit(err,"Error setting video mode");

		err = dc1394_video_set_framerate(cameras[i], framerate);
		clean_and_exit(err,"Error setting framerate");

		err = dc1394_capture_setup(cameras[i],CAMERA_BUFFER,DC1394_CAPTURE_FLAGS_DEFAULT);
		if(err!=DC1394_SUCCESS){
			//guarentees camera resources to be free
			dc1394_iso_release_bandwidth (cameras[i],1600);
			dc1394_iso_release_channel (cameras[i], list->ids[i].unit);
			clean_and_exit(err,"Error on setup");
		}

		if(AUTO_WHITE_BALANCE){
			dc1394_get_control_register(cameras[i],(uint64_t)WHITE_BALANCE_OFFSET,&white_reg);
			white_reg |= AUTO_WHITE_BALANCE_REG;
			dc1394_set_control_register(cameras[i],WHITE_BALANCE_OFFSET,white_reg);
		}else{
			dc1394_get_control_register(cameras[i],(uint64_t)WHITE_BALANCE_OFFSET,&white_reg);
			white_reg &= !AUTO_WHITE_BALANCE_REG;
			dc1394_set_control_register(cameras[i],WHITE_BALANCE_OFFSET,white_reg);
		}

		if(AUTO_BRIGHTNESS){
			dc1394_get_control_register(cameras[i],(uint64_t)BRIGHTNESS_OFFSET,&white_reg);
			white_reg |= AUTO_BRIGHTNESS_REG;
			dc1394_set_control_register(cameras[i],BRIGHTNESS_OFFSET,white_reg);
		}else{
			dc1394_get_control_register(cameras[i],(uint64_t)BRIGHTNESS_OFFSET,&white_reg);
			white_reg &= !AUTO_BRIGHTNESS_REG;
			dc1394_set_control_register(cameras[i],BRIGHTNESS_OFFSET,white_reg);
		}

		if(AUTO_EXPOSURE){
			dc1394_get_control_register(cameras[i],(uint64_t)AUTO_EXPOSURE_OFFSET,&white_reg);
			white_reg |= AUTO_EXPOSURE_REG;
			dc1394_set_control_register(cameras[i],AUTO_EXPOSURE_OFFSET,white_reg);
		}else{
			dc1394_get_control_register(cameras[i],(uint64_t)AUTO_EXPOSURE_OFFSET,&white_reg);
			white_reg &= !AUTO_EXPOSURE_REG;
			dc1394_set_control_register(cameras[i],AUTO_EXPOSURE_OFFSET,white_reg);
		}

		dc1394_camera_set_broadcast(cameras[i],(dc1394bool_t)1);
	}

	for (int i=0;i<list->num;i++){
		//starts cameras
		err = dc1394_video_set_transmission(cameras[i], DC1394_ON);
		clean_and_exit(err,"Error on starting camera");
	}
	//possibly need to wait until transmission is started.
}
/**
 * @brief
 */
void Camera::clean_and_exit(dc1394error_t err, string msg)
{
	if (err == DC1394_SUCCESS){
		//cout<<"SUCCESS"<<endl;
		return;
	}
	cout << msg<<endl;
	clean();
	exit(1);
}
/**
 * @brief Stops capturing, transmission and releases the cameras and iso channel
 */
void Camera::clean()
{
	for(int i =0;i<2;i++){
		dc1394_capture_stop( cameras[i] );
		dc1394_video_set_transmission( cameras[i], DC1394_OFF );
		dc1394_iso_release_all(cameras[i]);
		dc1394_camera_free (cameras[i] );
	}
	dc1394_free(dc_type);
}
/**
 * @brief Gets the currently set width
 */
int Camera::getImageWidth()
{
	return width;
}
/**
 * @brief Gets the currently set height
 */
int Camera::getImageHeight()
{
	return height;
}
/**
 * @brief Signals both cameras to enqueue the buffer if
 * the frames are null, else it dequeues the current buffer
 * this function implements the captureMutex!
 *@warning Uses mutex!
 */
void Camera::capture()
{
	pthread_mutex_lock(&captureMutex);
	dc1394error_t err;
	if(leftframe !=NULL && rightframe != NULL){
		err = dc1394_capture_enqueue(cameras[0],leftframe);
		clean_and_exit(err,"Error enqueue");
		err = dc1394_capture_enqueue(cameras[1],rightframe);
		clean_and_exit(err,"Error enqueue");
	}

	err = dc1394_capture_dequeue(cameras[0],DC1394_CAPTURE_POLICY_WAIT,&leftframe);
	clean_and_exit(err,"Error dequeue");
	err = dc1394_capture_dequeue(cameras[1],DC1394_CAPTURE_POLICY_WAIT,&rightframe);
	clean_and_exit(err,"Error dequeue");
	pthread_mutex_unlock(&captureMutex);

}
/**
 * @brief uses memcopy to retrieve the images.
 * @warning Uses the captureMutex!
 */
void Camera::download(unsigned char * left, unsigned char * right )
{
	pthread_mutex_lock(&captureMutex);
	memcpy(left, leftframe->image, height*width*3);
	memcpy(right, rightframe->image, height*width*3);
	pthread_mutex_unlock(&captureMutex);
}
////////////////////////////////////////////////////
//                                                //
//Chris these are the functions necessary to      //
//download and resize a single frame.             //
//if you touch them ill kill u dead!!! RAWR       //
////////////////////////////////////////////////////
/**
 * @brief uses memcopy to retrieve the left frame
 * @warning Uses the captureMutex
 */
void Camera::downloadLeft(unsigned char* left)
{
	pthread_mutex_lock(&captureMutex);
	memcpy(left,leftframe->image, height*width*3);
	pthread_mutex_unlock(&captureMutex);
}
/**
 * @brief Resizes an image using Magick++ to 256*192
 * @warning imageBuffer must be 256*192*3
 * @warning expects that the image copied is RGB 24bit 1024*768
 */
void Camera::getFrameAndResize(unsigned char* imageBuffer)
{
	/*	unsigned char *leftImage = (unsigned char*)malloc(1024*768*3);
		downloadLeft(leftImage);
		Magick::Image aimage;
		aimage.read(1024,768,"RGB",Magick::CharPixel,leftImage);
		aimage.scale(Magick::Geometry(256,192,0,0));
		aimage.write(0,0,256,192,"RGB",Magick::CharPixel,imageBuffer);
*/
}

