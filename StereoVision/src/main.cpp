#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fstream>

using namespace std;

// includes for OpenGL
#include <GL/glew.h>
#include <GL/glut.h>

#include <StereoCUDA.h>
#include <SOM_SA.h>
#include <HoughCPU.h>
//#include <StereoVision.h>
//#include <RectifierHost.h>

//#include <calibration.h>
#include <camera.h>  // For the Point Grey stereo Camera
Camera ptGreyCam;  // the object for the camera

// Camera aquisition and processing parameters
static unsigned ImageWidth = 1024;
static unsigned ImageHeight = 768;

static GLuint LeftImageBuffer = 0;			// This is an identifier of a OpenGL Buffer Object which will hold the image to be displayed
static GLuint RightImageBuffer = 0;
static GLuint DispImageBuffer = 0;
static GLuint DispImageBuffer2 = 0;

static GLuint LeftImageTexture = 0;        // We also need an OpenGL texture object to draw the image.  We will bind the data referenced by ImageBuffer to this texture
static GLuint RightImageTexture = 0;
static GLuint DispImageTexture = 0;
static GLuint DispImageTexture2 = 0;

static GLuint GLWindowWidth = 800;
static GLuint GLWindowHeight = 600;

unsigned char * leftImg;
unsigned char * rightImg;
unsigned char *leftdis2;
unsigned char *segImg2;
double **prob_map;
StereoCUDA *stereoVision;
//RectifierHost *rectifier;

#define MAX_WIDTH  3648
#define MAX_LENGTH  2736

static int frame_count;  // counts the number of frames for timing
static float time_avg;

// forward declerations for glut callback functions
void display(void);
void idle(void);
void keyboard( unsigned char key, int x, int y);
void reshape (int x, int y);

bool use_camera =
		true;
int counter = 1;



typedef struct distance_info{
	double y;
	double theta;
	double x;
}distance_info_t;

distance_info_t **distances;

void getTestImage(char *file_name, char* imagebuffer,int img_w, int img_h){
	IplImage* img=0;
	img=cvLoadImage(file_name);
	if(!img){
		printf("Could not load image file: %s\n",file_name);
		exit(1);
	}
//	cout<<"channels: "<<img->nChannels<<endl;
	memcpy(imagebuffer, img->imageData, img_h*img_w*3);
}

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main( int argc, char** argv) {



   	// First parse command line to see if we are using a camera or file
  	if(argc > 1) {
	   	if(strcmp( "-c",argv[1]) == 0) 
	   	{
		//	CameraCalibration *calib = new CameraCalibration();
		//	calib->calibrate(1);
			exit(0);
	   	}else if(strcmp("-h",argv[1])==0){
	//   		testHough("../Images/lines193.pgm");
	   		exit(0);
	   	}else if(strcmp("-t",argv[1])==0){
	   	/*	StereoVision *testVision = new StereoVision();
	   		pthread_t thread;
	   		long rc = pthread_create(&thread,NULL, testVision->run, NULL);
	   	    if (rc){
	   	      printf("ERROR; return code from pthread_create() is %d\n",rc);
	   	      return -1;
	   	    }
	   	    sleep(15);
	   	    testVision->kill();
	   	    pthread_exit(NULL);*/
	   	}
	}
   	// Allocate GPU memory
  	cout<<"allocating memory"<<endl;
   	stereoVision = new StereoCUDA(ImageWidth, ImageHeight);
 //  	rectifier = new RectifierHost(ImageWidth, ImageHeight);
   	cout<<"done allocating gpu mem"<<endl;

	// Allocate CPU memory for the point grey camera 
	leftImg = (unsigned char *)malloc(ImageWidth*ImageHeight*3);
	rightImg = (unsigned char *)malloc(ImageWidth*ImageHeight*3);
	leftdis2 = (unsigned char*)malloc(ImageWidth*ImageHeight*sizeof(unsigned char));
	segImg2 = (unsigned char*)malloc(ImageWidth*ImageHeight*sizeof(unsigned char));
	distances = (distance_info_t**)malloc(sizeof(distance_info_t*)*ImageWidth);
	for(int i=0;i<ImageWidth;i++){
		distances[i] = (distance_info_t*)malloc(sizeof(distance_info_t)*ImageHeight);
	}
	prob_map = (double**)malloc(sizeof(double*) * MAX_LENGTH);
 	for(int i=0;i < MAX_LENGTH;i++){ 
		prob_map[i] = (double*)malloc(sizeof(double) * MAX_WIDTH);
  	}	
   
	//ptGreyCam.initialize(ImageWidth,ImageHeight); //comment out without cameras




	// Now we are going to setup the OpenGL Utility Library Stuff to create our window and respond to user I/O
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(GLWindowWidth, GLWindowHeight);
	glutCreateWindow("Stereo Disparity Map Computation Flow");
	glutDisplayFunc(display);		// set function pointer to call on display
	glutKeyboardFunc(keyboard);		// set function pointer to call on keyboard input
	glutReshapeFunc(reshape);
	glutIdleFunc(idle);				// set function pointer to call on Idle
	glewInit(); // initi the extension wrangler to allow use of the buffer objects
	if (!glewIsSupported("GL_VERSION_2_0 GL_VERSION_1_5 GL_ARB_vertex_buffer_object GL_ARB_pixel_buffer_object")) {
		fprintf(stderr, "Required OpenGL extensions missing.");
		exit(-1);
	}


	// Now we need to create a couple memory buffers for drawing.  OpenGL
	// and CUDA will share these buffers. OpenGL Buffer Objects are simply 
	// blocks of memory allocated on the Graphics card.  These buffers can contain
	// pixel data, vertex data, etc.  Using CUDA's OpenGL Interop capabilities
	// these buffers become accessable for read/write access.  When done, the buffers
	// can be returned to OpenGL control and used to draw whatever was placed into them

	// The following code is all OpenGL stuff to create and allocate the buffers, no CUDA yet.
	
	// Create a set of OpenGL Buffers to hold the image data we wish to draw	
	unsigned int size = ImageWidth * ImageHeight *  sizeof(GLubyte)*3 ;  // Image will be in 8-bit greyscale format
	glGenBuffers( 1, &LeftImageBuffer);  // This creates the buffer
	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, LeftImageBuffer);  // this make the buffer current 	
	glBufferData( GL_PIXEL_UNPACK_BUFFER, size, NULL, GL_DYNAMIC_DRAW);  // this allocates the proper size and sets the data null
	glGenBuffers( 1, &RightImageBuffer);  
	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, RightImageBuffer);  
	glBufferData( GL_PIXEL_UNPACK_BUFFER, size, NULL, GL_DYNAMIC_DRAW);  

	size = ImageWidth * ImageHeight *  sizeof(GLubyte) * 4;
	glGenBuffers( 1, &DispImageBuffer);  
	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, DispImageBuffer);   	
	glBufferData( GL_PIXEL_UNPACK_BUFFER, size, NULL, GL_DYNAMIC_DRAW);  
	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, 0);  // this make the buffer current

	size = ImageWidth* ImageHeight*  sizeof(GLubyte);
	glGenBuffers( 1, &DispImageBuffer2);
	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, DispImageBuffer2);
	glBufferData( GL_PIXEL_UNPACK_BUFFER, size, NULL, GL_DYNAMIC_DRAW);
	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, 0);  // this make the buffer current 


	// Left Image Texture
	glGenTextures( 1, &LeftImageTexture);  // Create the texture reference
	glBindTexture( GL_TEXTURE_2D, LeftImageTexture);    // Make it current
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, ImageWidth, ImageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL); // Actually allocate the memory
	// Right Image Texture
	glGenTextures( 1, &RightImageTexture);  // Create the texture reference
	glBindTexture( GL_TEXTURE_2D, RightImageTexture);    // Make it current
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, ImageWidth, ImageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL); // Actually allocate the memory
	// Disparity Map Texture
	glGenTextures( 1, &DispImageTexture);  // Create the texture reference
	glBindTexture( GL_TEXTURE_2D, DispImageTexture);    // Make it current
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, ImageWidth, ImageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL); // Actually allocate the memory
	
	// Disparity2 Map Texture
	glGenTextures( 1, &DispImageTexture2);  // Create the texture reference
	glBindTexture( GL_TEXTURE_2D, DispImageTexture2);    // Make it current
	glTexImage2D( GL_TEXTURE_2D, 0, GL_LUMINANCE, ImageWidth, ImageHeight, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL); // Actually allocate the memory

	glBindTexture(GL_TEXTURE_2D, 0);
	
	// pass these buffer ID's for the stereo algorithm and register them for CUDA interop
	stereoVision->SetDisplayImageBuffers(LeftImageBuffer,RightImageBuffer,DispImageBuffer,DispImageBuffer2);
	
	// zero the timer variables
	time_avg = 0;
	frame_count = 0;
	
	glutMainLoop();  // that's all the setup!  now kick off the main loop


}



void reshape(int x, int y) {
    glViewport(0, 0, x, y);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 1, 1, 0, -1, 1); 
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glutPostRedisplay();
}



/* This is the main function where the stereo process is kicked off */
void display(void) {  
	int ImgType;
	// count how many times we run for timing

	// !!This where image aquisition occurs!!

	// Get a frame.  For the point grey camera, the pointers to host memory 
	// where the images get stored were set during the call to ptGreyCam::init 
	// in the main function.
	
	// no need to assign pointers here, it's automatic since we gave fixed
	// address to the point grey driver
	//ptGreyCam.capture(); //comment out without cameras
	leftImg[0]='\0';
	rightImg[0]='\0';
	//ptGreyCam.download(leftImg,rightImg); //comment out without cameras
	char leftImg_name[255], rightImg_name[255], segImg_name[255];
	leftImg_name[0]='\0';
	segImg_name[0]='\0';
	rightImg_name[0]='\0';


	sprintf(rightImg_name,"/home/cgamache/Stereo vision/Paper/test%dr.jpg",counter);
	sprintf(segImg_name,"/home/cgamache/Stereo vision/Paper/testSeg%d.pgm",counter);
	sprintf(leftImg_name,"/home/cgamache/Stereo vision/Paper/test%dr.jpg",counter);
	counter+=1;
	getTestImage(leftImg_name,(char*)leftImg,ImageWidth,ImageHeight); //comment out with cameras
	getTestImage(rightImg_name,(char*)rightImg,ImageWidth,ImageHeight); //comment out with cameras

	if(leftImg==NULL || rightImg ==NULL) cout<<"zomg wtf"<<endl;

	//rectifier->rectify(leftImg, rightImg);
	time_avg += stereoVision->stereoProcess(rightImg, leftImg,leftdis2,segImg2);
	saveImage("tesregImage",counter,rightImg,3,ImageWidth,ImageHeight);
	saveImage("testsegImage",counter,segImg2,1,ImageWidth,ImageHeight);
	//houghTransform(segImg2);
	//saveImage("segtest",counter,segImg2,1,201,101);
	//saveImage("right",counter,rightImg,3,ImageWidth,ImageHeight);
	//saveImage("left",counter,leftImg,3,ImageWidth,ImageHeight);
	//printToFile("../Images/seg_img.txt",segImg,201,101);

	// Now we Draw!
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glClear(GL_COLOR_BUFFER_BIT);  // Clear the screen

	glBindTexture( GL_TEXTURE_2D, LeftImageTexture);  // Select the Image Texture
    	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, LeftImageBuffer); // And Bind the OpenGL Buffer for the pixel data 
    	glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, ImageWidth, ImageHeight, GL_RGB, GL_UNSIGNED_BYTE, 0);  // Set the texture parameters
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);  // then unbind
   
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Now just draw a square on the screen and texture it with the image texture
	glBegin( GL_QUADS);		
		glVertex3f(0.0,0.0,0.5);
		glTexCoord2f(1.0, 0.0);	 
		
		glVertex3f(0.5,0.0,0.5);
		glTexCoord2f(1.0, 1.0);
		
		glVertex3f(0.5,0.5,0.5);
		glTexCoord2f( 0.0, 1.0);	 
		
		glVertex3f(0.0,0.5,0.5);
		glTexCoord2f( 0.0,0.0);	
				
	glEnd();

	// right image
	glBindTexture( GL_TEXTURE_2D, RightImageTexture);  // Select the Image Texture
    	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, RightImageBuffer); // And Bind the OpenGL Buffer for the pixel data 
    	glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, ImageWidth, ImageHeight, GL_RGB, GL_UNSIGNED_BYTE, 0);  // Set the texture parameters
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);  // then unbind
   
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin( GL_QUADS);	
		glVertex3f(0.5,0.0,0.5);
		glTexCoord2f( 1.0, 0.0);	 
		
		glVertex3f(1.0,0.0,0.5);
		glTexCoord2f(1.0, 1.0);
		
		glVertex3f(1.0,0.5,0.5);
		glTexCoord2f( 0.0, 1.0);	 
		
		glVertex3f(0.5,0.5,0.5);
		glTexCoord2f( 0.0,0.0);	
				
	glEnd();

	// disparity map
	glBindTexture( GL_TEXTURE_2D, DispImageTexture);  // Select the Image Texture
    	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, DispImageBuffer); // And Bind the OpenGL Buffer for the pixel data 
    	glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, ImageWidth, ImageHeight, GL_RGBA, GL_UNSIGNED_BYTE, 0);  // Set the texture parameters
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);  // then unbind

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin( GL_QUADS);
		glVertex3f(0.5,0.5,0.5);
		glTexCoord2f( 1.0, 0.0);

		glVertex3f(1.0,0.5,0.5);
		glTexCoord2f(1.0, 1.0);

		glVertex3f(1.0,1.0,0.5);
		glTexCoord2f( 0.0, 1.0);

		glVertex3f(0.5,1.0,0.5);
		glTexCoord2f( 0.0,0.0);

	glEnd();

	// disparity map
	glBindTexture( GL_TEXTURE_2D, DispImageTexture2);  // Select the Image Texture
    	glBindBuffer( GL_PIXEL_UNPACK_BUFFER, DispImageBuffer2); // And Bind the OpenGL Buffer for the pixel data
    	glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, ImageWidth, ImageHeight, GL_LUMINANCE, GL_UNSIGNED_BYTE, 0);  // Set the texture parameters
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);  // then unbind

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin( GL_QUADS);	
		glVertex3f(0.0,0.5,0.0);
		glTexCoord2f( 1.0, 0.0);	 
		
		glVertex3f(0.5,0.5,0.5);
		glTexCoord2f(1.0, 1.0);
		
		glVertex3f(0.5,1.0,0.5);
		glTexCoord2f( 0.0, 1.0);	 
		
		glVertex3f(0.0,1.0,0.5);
		glTexCoord2f( 0.0,0.0);	
				
	glEnd();
	
	glBindTexture( GL_TEXTURE_2D, 0);  // Select the Image Texture
    	glutSwapBuffers();
	frame_count++;
	if(frame_count >= 1)
	{
		char s[256];
        sprintf(s, "Stereo Time: %3.3f ms/frame",time_avg);  
		cout<<s<<endl;
		time_avg = 0;
		frame_count = 0;
	}
    glutPostRedisplay();
    sleep(2);
}

void idle(void) {
    glutPostRedisplay();
}

void keyboard( unsigned char key, int x, int y) {
    switch( key) {
        case 27:
        	stereoVision->UnregisterGLBufferForCUDA(LeftImageBuffer);
        	stereoVision->UnregisterGLBufferForCUDA(RightImageBuffer);
        	stereoVision->UnregisterGLBufferForCUDA(DispImageBuffer);
        	ptGreyCam.~Camera();
        	if(use_camera)  // only need to free memory if we are using the point grey camera.  Open CV manages it's own
        	{
        		if(leftImg != NULL) free(leftImg);
        		if(rightImg != NULL) free(rightImg);
        	}
        	stereoVision->~StereoCUDA();
        	exit(1);
			break;		
        default: break;
    }
    glutPostRedisplay();
}
