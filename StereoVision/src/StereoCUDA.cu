/**
 * @author Christopher Gamache
 * @date 03/14/2010
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;
#define DEBUG 1//turns on drawing


#include <math.h>
#include <StereoCUDA.h>


#include <gpu.hpp>
#include <random.hpp>

#define SQ(a) (__mul24(a,a))  // sad or ssd

//Declaration of the textures used for accessing the image data
texture<unsigned char, 2, cudaReadModeNormalizedFloat> leftTex;
texture<unsigned char, 2, cudaReadModeNormalizedFloat> rightTex;


/********
The function SetupStereo allocates GPU memory and sets critical parameters in stored in global variable.
We pass the width & height into the function, but let CUDA allocate the host memory.
Memory is allocated as pinned using cudaMallocHost or traditionally using malloc.
Pinned memory can provide nearly double the transfer speed to the GPU, but is not always compatible
with the video capture source, as it the case with the Point Grey camera driver.
*********/
StereoCUDA::StereoCUDA(unsigned int w, unsigned int h)
{

	g_w = w;
	g_h = h;


	CUDA_SAFE_CALL(cudaMallocPitch((void**)&g_disparityLeft,&g_floatDispPitch,w*sizeof(float),h));
	CUDA_SAFE_CALL(cudaMallocPitch((void**)&g_disparityLeft2,&g_floatDispPitch,w*sizeof(float),h));
	CUDA_SAFE_CALL(cudaMallocPitch((void**)&g_minSSD, &g_floatDispPitch,w*sizeof(int),h));
	g_floatDispPitch /= sizeof(float);

	CUDA_SAFE_CALL(cudaMallocPitch((void**)&LuvImageLeft,&Luv_pitch,g_w*sizeof(float3),g_h));
	CUDA_SAFE_CALL(cudaMallocPitch((void**)&LuvImageRight,&Luv_pitch,g_w*sizeof(float3),g_h));
	CUDA_SAFE_CALL(cudaMallocPitch((void**)&RightImage,&RGB_pitch,g_w*sizeof(uchar3),g_h));
	CUDA_SAFE_CALL(cudaMallocPitch((void**)&LeftImage,&RGB_pitch,g_w*sizeof(uchar3),g_h));

	Luv_pitch = Luv_pitch/sizeof(float3);
	RGB_pitch = RGB_pitch/sizeof(uchar3);
	SOM_MAP = new SOM_SA(w,h);
	rectifier = new Rectifier(w,h);
//	mapper = new StereoMapper(w,h);
	lineDetector = new LineDetector(w,h);
	SOM_TRAINED=0;

	cudaChannelFormatDesc U8Tex = cudaCreateChannelDesc<unsigned char>();
	cudaMallocArray(&g_leftTex_array, &U8Tex, g_w, g_h);
	cudaMallocArray(&g_rightTex_array, &U8Tex, g_w, g_h);

	print_GPU_mem();
}

/*********
SetDisplayImageBuffers
Simply sets the OpenGL buffer IDs and registers them for CUDA GLinterop
*********/
__host__ void StereoCUDA::SetDisplayImageBuffers (unsigned int Left, unsigned int Right,unsigned int Disparity,unsigned int Disparity2)
{
	LeftImage_GLBufferID = Left;
	RightImage_GLBufferID = Right;
	DisparityImage_GLBufferID = Disparity;
	DisparityImage2_GLBufferID = Disparity2;

	cudaGLRegisterBufferObject(LeftImage_GLBufferID);
	cudaGLRegisterBufferObject(RightImage_GLBufferID);
	cudaGLRegisterBufferObject(DisparityImage_GLBufferID);
	cudaGLRegisterBufferObject(DisparityImage2_GLBufferID);
}

/**********
Used at cleanup to unregister OpenGL buffers
**********/
__host__ void StereoCUDA::UnregisterGLBufferForCUDA(int buffer)
{
    CUDA_SAFE_CALL(cudaGLUnregisterBufferObject(buffer));
}

/*
 * @brief Converts a 24bit RGB image to modified L*u*v* using conversions given in (FIXME)paper.  The conversion is performed in the GPU and needs to generate one thread per image pixel.
 * @param src  The source image given in 24 bit RGB
 * @param dest The destination L*u*v* image in float3 format (must have already allocated space on the GPU for this image)
 * @param src_pitch The pitch associated with the source image
 * @param dest_pitch The pitch associated with the destination image
 * @param width Width of the original source image
 * @param height Height of the original source image
 * @warning Need to generate as many threads as there are pixels
 */
__global__ void convertRGB_to_MLUV(uchar3 *src, float3 * dest, size_t src_pitch, size_t dest_pitch, int width, int height){

	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	uchar3 in_val;
	float XYZ_t[3][3] = {{.412453,.35758,.180423},{.212671,.715160,.072169},{.019334,.119193,.950227}};
	float uv_w[2] = {.1978,.4683};
	float XYZ[3];//stores X, Y, and Z
	float uv_p[2];//stores u' and v'
	float LUV[3];
	float uv_div;
	float3 out_val;

	if((y < height) && (x < width))
	{
		in_val = *((uchar3*)(&src[__mul24((height-y-1),src_pitch) + x]));

		for(int i=0;i<3;i++)
			XYZ[i] = (float)in_val.x * XYZ_t[i][0] + (float)in_val.y * XYZ_t[i][1] + (float)in_val.z * XYZ_t[i][2];

		uv_div = XYZ[0] + 15*XYZ[1] + 3*XYZ[2];
		if(uv_div==0) uv_div=1;
		uv_p[0] = 4*XYZ[0] / uv_div;
		uv_p[1] = 9*XYZ[1] / uv_div;

		LUV[0] = 10*sqrt(XYZ[1]);
		LUV[1] = 13*LUV[0]*(uv_p[0] - uv_w[0]);
		LUV[2] = 13*LUV[0]*(uv_p[1] - uv_w[1]);

		out_val.x = LUV[0];
		out_val.y = LUV[1];
		out_val.z = LUV[2];
		dest[__mul24((height-y-1),dest_pitch)+x] = out_val;
	}

}

/**********
drawDisparityKernel()
This function converts the floating point disparity values into a false color RGBA image.
The image is allocated as an OpenGL buffer from the main program
*********/

#define GAIN (1.0f / (256))

__global__ void drawDisparityKernel(	uchar4 * out_image,
										size_t out_pitch,
										float *disparity,
										size_t disparity_pitch,
										int width,
										int height)
{
    const int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
    const int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	uchar4 temp;
    if(x < width && y < height) {
        float d = disparity[__mul24(y,disparity_pitch)+x];

		// first draw unmatched pixels in grey
        if(d == STEREO_MIND-1)
		{
			temp.x = 127;
			temp.y = 127;
			temp.z = 0;
			temp.w = 255;
		}
		else
		{
			d = d+128;
           float val = ((float)d)*GAIN;
		   float r = 1.0f;
		   float g = 1.0f;
		   float b = 1.0f;
		   if (val < 0.25f) {
			  r = 0;
			  g = 4.0f * val;
		   } else if (val < 0.5f) {
			  r = 0;
			  b = 1.0 + 4.0f * (0.25f - val);
		   } else if (val <  0.75f) {
			  r = 4.0f * (val - 0.5f);
			  b = 0;
		   } else {
			  g = 1.0f + 4.0f * (0.75f - val);
			  b = 0;
		   }
			temp.x = 255 * r;
			temp.y = 255 * g;
			temp.z = 255 * b;
			temp.w = 255;
			/*if(d < 50){//pink
				temp.x=255;
				temp.y=0;
				temp.z=255;
			}else if(d<100){//lightblue
				temp.x=0;
				temp.y=255;
				temp.z=255;
			}else if(d<150){//yrllow
				temp.x=255;
				temp.y=255;
				temp.z=0;
			}else if(d<200){//red
				temp.x=255;
			}else if(d<250){//blue
				temp.y=255;
			}else if(d<350){//green
				temp.z=255;
			}*/
		}
		out_image[__mul24(y,out_pitch)+x] = temp;
    }



}

/*******
stereoKernel
Now for the main stereo kernel:  There are five parameters:
disparityPixel & disparityMinSSD point to memory containing the disparity value (d)
and the current minimum sum-of-squared-difference values for each pixel.
width & height are the image width & height, and out_pitch specifies the pitch of the output data in words
(i.e. the number of floats between the start of one row and the start of the next.).
*********/

__global__ void stereoKernel(float *disparityPixel,   // pointer to the output memory for the disparity map
							 int *disparityMinSSD,
							 int width,
							 int height,
							 size_t out_pitch)		 // the pitch (in pixels) of the output memory for the disparity map
{


	extern __shared__ int col_ssd[]; // column squared difference functions

	float d;     // disparity value
	int diff;  // difference temporary value
	int ssd;   // total SSD for a kernel
	float x_tex; // texture coordinates for image lookup
	float y_tex;
	int row;	 // the current row in the rolling window
	int i;       // for index variable

	// use define's to save registers
    #define X (__mul24(blockIdx.x,BLOCK_W) + threadIdx.x)
	#define Y (__mul24(blockIdx.y,ROWSperTHREAD))

	// for threads reading the extra border pixels, this is the offset
	// into shared memory to store the values

	int extra_read_val = 0;
	if(threadIdx.x < (2*RADIUS_H)) extra_read_val = BLOCK_W+threadIdx.x;

	// initialize the memory used for the disparity and the disparity difference
	if(X<width )
	{
		for(i = 0;i<ROWSperTHREAD && Y+i < height;i++)
		{
			disparityPixel[__mul24((Y+i),out_pitch)+X] = STEREO_MIND-1;  // initialize to -1 indicating no match
			disparityMinSSD[__mul24((Y+i),out_pitch)+X] = MIN_SSD;
		}
	}
	__syncthreads();

	x_tex = X - RADIUS_H;
	for(d = STEREO_MIND; d <= STEREO_MAXD; d += STEREO_DISP_STEP)
	{

		col_ssd[threadIdx.x] = 0;
		if(extra_read_val>0)	col_ssd[extra_read_val] = 0;

		// do the first row
		y_tex = Y - RADIUS_V;

		for(i = 0; i <= 2*RADIUS_V; i++)
		{
				diff = (int)(255.0f*tex2D(leftTex,x_tex,y_tex)) - (int)(255.0f*tex2D(rightTex,x_tex-d,y_tex));
				col_ssd[threadIdx.x] += SQ(diff);


				if(extra_read_val > 0)
				{
					diff = (int)(255.0f*tex2D(leftTex,x_tex+BLOCK_W,y_tex)) - (int)(255.0f*tex2D(rightTex,x_tex+BLOCK_W-d,y_tex));
					col_ssd[extra_read_val] += SQ(diff);
				}
			y_tex += 1.0f;
		}
		__syncthreads();

		// now accumulate the total
		if(X < width && Y < height)
		{
			ssd = 0;
			for(i = 0;i<=(2*RADIUS_H);i++)
			{
				ssd += col_ssd[i+threadIdx.x];
			}

			// the 1.0e-5 factor is a bias due to floating point error in the accumulation of the SSD value
			if( ssd  < disparityMinSSD[__mul24(Y,out_pitch) + X])
			{
				disparityPixel[__mul24(Y,out_pitch) + X] = d;
				disparityMinSSD[Y*out_pitch + X] = ssd;
			}
		}
		__syncthreads();

		// now do the remaining rows
		y_tex = Y - RADIUS_V; // this is the row we will remove
		for(row = 1;row < ROWSperTHREAD && (row+Y < (height+RADIUS_V)); row++)
		{
				// subtract the value of the first row from column sums
				diff = (int)(255.0f*tex2D(leftTex,x_tex,y_tex)) - (int)(255.0f*tex2D(rightTex,x_tex-d,y_tex));
				col_ssd[threadIdx.x] -= SQ(diff);

				// add in the value from the next row down
		   	 	diff = (int)(255.0f*tex2D(leftTex,x_tex,y_tex + (float)(2*RADIUS_V)+1.0f)) - (int)(255.0f*tex2D(rightTex,x_tex-d,y_tex + (float)(2*RADIUS_V)+1.0f));
				col_ssd[threadIdx.x] += SQ(diff);

				if(extra_read_val > 0)
				{
					diff =  (int)(255.0f*tex2D(leftTex,x_tex+(float)BLOCK_W,y_tex)) - (int)(255.0f*tex2D(rightTex,x_tex-d+(float)BLOCK_W,y_tex));
					col_ssd[extra_read_val] -= SQ(diff);

					diff = (int)(255.0f*tex2D(leftTex,x_tex+(float)BLOCK_W,y_tex + (float)(2*RADIUS_V)+1.0f)) - (int)(255.0f*tex2D(rightTex,x_tex-d+(float)BLOCK_W,y_tex + (float)(2*RADIUS_V)+1.0f));
					col_ssd[extra_read_val] += SQ(diff);
				}
			y_tex += 1.0f;
		    __syncthreads();

			if(X<width && (Y+row) < height)
			{
				ssd = 0;

				for(i = 0;i<=(2*RADIUS_H);i++)
				{
					ssd += col_ssd[i+threadIdx.x];
				}
				if(ssd < disparityMinSSD[__mul24(Y+row,out_pitch) + X])
				{
					disparityPixel[__mul24(Y+row,out_pitch) + X] = d;
					disparityMinSSD[__mul24(Y+row,out_pitch) + X] = ssd;
				}
			}
			__syncthreads();  // wait for everything to complete
		} // for row loop
	} // for d loop

}

__global__ void RGB_to_grey(unsigned char* left_grey, unsigned char* right_grey, uchar3* left_rgb, uchar3* right_rgb, size_t img_pitch, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	uchar3 tmp;

	if((y < height) && (x < width)){
		tmp = left_rgb[__mul24(y,img_pitch)+x];
		//if(tmp.x==0 && tmp.y==255 && tmp.z ==0) left_grey[__mul24(y,img_pitch)+x]=0;
		left_grey[__mul24(y,img_pitch)+x] = (11*tmp.x +16*tmp.y+5*tmp.z)/32;

		tmp = right_rgb[__mul24(y,img_pitch)+x];
		//if(tmp.x==0 && tmp.y==255 && tmp.z ==0) right_grey[__mul24(y,img_pitch)+x]=0;
		right_grey[__mul24(y,img_pitch)+x] = (11*tmp.x +16*tmp.y+5*tmp.z)/32;
	}


}

void RGBA_to_RGB(uchar4* org_img, uchar3* dest_img, size_t pitch, int width, int height){
	uchar3 temp;
	uchar4 org_temp;

	for(int i=0;i<height;i++)
		for(int j=0;j<width;j++){
			org_temp = org_img[i*pitch +j];
			temp.x = org_temp.x;
			temp.y =org_temp.y;
			temp.z = org_temp.z;
			dest_img[i*pitch +j]=temp;
		}
}

__global__ void BGR_to_RGB(uchar3* img, size_t pitch, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	uchar3 temp, final;

	if((y < height) && (x < width)){
		temp= img[mul24(y,pitch)+x];
		final.x=temp.z;
		final.y=temp.y;
		final.z=temp.x;
		img[mul24(y,pitch)+x]=final;
	}
}


float StereoCUDA::stereoProcess(unsigned char * p_hostLeft,unsigned char * p_hostRight, unsigned char *gray_scale_image, unsigned char* seg_image){
	unsigned int timer;
	unsigned char* left_grey, *right_grey;
	size_t grey_pitch;
	CUDA_SAFE_CALL(cudaMallocPitch((void**)&left_grey,&grey_pitch,g_w,g_h));
	CUDA_SAFE_CALL(cudaMallocPitch((void**)&right_grey,&grey_pitch,g_w,g_h));


	unsigned char * GLtemp;

	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(g_w,threads.x);
	grid.y = divUp(g_h,threads.y);

	//rectifier->rectify((uchar3*)p_hostLeft, (uchar3*)p_hostRight, LeftImage, RightImage);

//	cudaMemcpy(p_hostLeft,LeftImage,g_w*sizeof(uchar3)*g_h,cudaMemcpyDeviceToHost);
	//cudaMemcpy(p_hostRight,RightImage,g_w*sizeof(uchar3)*g_h,cudaMemcpyDeviceToHost);
	cudaMemcpy(LeftImage,p_hostLeft,g_w*sizeof(uchar3)*g_h,cudaMemcpyHostToDevice);
	cudaMemcpy(RightImage,p_hostRight,g_w*sizeof(uchar3)*g_h,cudaMemcpyHostToDevice);
 	//rectifier->printRectification(LeftImage,RightImage);

	BGR_to_RGB<<<grid,threads>>>(LeftImage,RGB_pitch,g_w,g_h);
	cudaThreadSynchronize();
	BGR_to_RGB<<<grid,threads>>>(RightImage,RGB_pitch,g_w,g_h);
	cudaThreadSynchronize();

	float3* LuvImageLeft_host = (float3*)malloc(sizeof(float3)*g_w*g_h);
	CUT_SAFE_CALL(cutCreateTimer(&timer));
	CUT_SAFE_CALL(cutStartTimer(timer));
	convertRGB_to_MLUV<<<grid,threads>>>(LeftImage,LuvImageLeft,RGB_pitch,Luv_pitch,g_w,g_h);
	cudaThreadSynchronize();
	//cudaMemcpy(LuvImageLeft_host,LuvImageLeft,g_w*sizeof(float3)*g_h,cudaMemcpyDeviceToHost);
	if(!SOM_TRAINED){
		//SOM_MAP->create_SOM(LuvImageLeft_host);
		SOM_MAP->loadSOM(SOM_FILENAME);

	}
	SOM_TRAINED++;
	//SOM_MAP->train(LuvImageLeft_host);
	//cout<<"creating som"<<endl;
	//SOM_MAP->create_SOM_paper_test(LuvImageLeft_host);
	unsigned char *neural_seg;
	SOM_MAP->segment(LuvImageLeft,Luv_pitch,&neural_seg);

	CUT_SAFE_CALL(cutStopTimer(timer));  // don't time the drawing
	float retval = cutGetTimerValue(timer);

	RGB_to_grey<<<grid,threads>>>(left_grey,right_grey,LeftImage,RightImage,RGB_pitch,g_w,g_h);
	cudaThreadSynchronize();

	cudaMemcpyToArray(g_leftTex_array, 0, 0, left_grey,g_w * g_h, cudaMemcpyDeviceToDevice);
	cudaMemcpyToArray(g_rightTex_array, 0, 0, right_grey,g_w * g_h, cudaMemcpyDeviceToDevice);
	unsigned char* temp_seg;
	cudaMemcpy(seg_image,neural_seg,g_w*sizeof(unsigned char)*g_h,cudaMemcpyDeviceToHost);
	SOM_MAP->print(SOM_TRAINED);
	//lineDetector->segmentLines(LuvImageLeft,&temp_seg);
	//lineDetector->detectLines(temp_seg,&seg_image);
//	lineDetector->detectCones(neural_seg);
	//lineDetector->drawBlocks(LeftImage); //visualization

	cudaGLMapBufferObject( (void**)&GLtemp, LeftImage_GLBufferID);
	cudaMemcpy(GLtemp,LeftImage,g_w*g_h*3,cudaMemcpyDeviceToDevice);
	cudaGLUnmapBufferObject(LeftImage_GLBufferID);
	cudaGLMapBufferObject( (void**)&GLtemp, RightImage_GLBufferID);
	cudaMemcpy(GLtemp,RightImage,g_w*g_h*3,cudaMemcpyDeviceToDevice);
	cudaGLUnmapBufferObject(RightImage_GLBufferID);

	// Set up the texture parameters for bilinear interpolation & clamping
	leftTex.filterMode = cudaFilterModeLinear;
	cudaBindTextureToArray(leftTex, g_leftTex_array);
	rightTex.filterMode = cudaFilterModeLinear;
	cudaBindTextureToArray(rightTex, g_rightTex_array);

	threads.x = BLOCK_W;
	threads.y = 1;
    grid.x = divUp(g_w, BLOCK_W);
	grid.y = divUp(g_h,ROWSperTHREAD);

	stereoKernel<<<grid,threads,SHARED_MEM_SIZE>>>(g_disparityLeft2,g_minSSD,g_w,g_h,g_floatDispPitch);
	cudaThreadSynchronize();

	threads.x = 16;
	threads.y = 8;
	grid.x = divUp(g_w , threads.x);
	grid.y = divUp(g_h , threads.y);
	uchar4 * DisparityMap;
	size_t DispPitch;

	cudaMallocPitch((void**)&DisparityMap,&DispPitch,g_w*sizeof(uchar4),g_h);
	drawDisparityKernel<<<grid,threads>>>(DisparityMap,DispPitch/sizeof(uchar4),g_disparityLeft2,g_floatDispPitch,g_w,g_h);
	cudaThreadSynchronize();

	// now copy the output for openGL
	cudaGLMapBufferObject( (void**)&GLtemp, DisparityImage2_GLBufferID);
	cudaMemcpy(GLtemp,neural_seg,g_w*g_h,cudaMemcpyDeviceToDevice);
	cudaGLUnmapBufferObject(DisparityImage2_GLBufferID);

	/*char *disparity_map_temp = (char*)malloc(sizeof(char)*g_h*g_w*4);
	char *disparity_map=(char*)malloc(sizeof(char)*g_h*g_w*3);
	cudaMemcpy(disparity_map_temp,DisparityMap,g_w*g_h*4,cudaMemcpyDeviceToHost);

	RGBA_to_RGB((uchar4*)disparity_map_temp, (uchar3*)disparity_map, RGB_pitch, g_w,g_h);
	FILE* imagefile_left;
	char leftImg_name[255];
	leftImg_name[0]='\0';


	sprintf(leftImg_name,"disparity_map_na.ppm");

	imagefile_left = fopen(leftImg_name,"wb");

	//write out left image
	fprintf( imagefile_left, "P6\n%u %u 255\n", g_w, g_h );
	fwrite( (const char *)disparity_map, 1,
			g_w* g_h*3, imagefile_left);
	fclose(imagefile_left);*/

	unsigned char *ret_seg_img;
	SOM_MAP->segmentBasedAverage(g_disparityLeft2,(unsigned char**)&ret_seg_img);
	cudaThreadSynchronize();

	//mapper->map2D((int*)NULL,g_disparityLeft2,RGB_pitch,g_w,g_h);


	//cudaUnbindTexture(leftTex);
	//cudaUnbindTexture(rightTex);

	//cudaBindTextureToArray(leftTex, g_rightTex_array);
	//cudaBindTextureToArray(rightTex, g_leftTex_array);

	//stereoKernel<<<grid,threads,SHARED_MEM_SIZE>>>(g_disparityLeft,g_minSSD,g_w,g_h,g_floatDispPitch);
	//cudaThreadSynchronize();

	CUT_CHECK_ERROR("Kernel execution failed writing disparity map!");

	//print map
	//float *host_disp = (float*) malloc(sizeof(float)*g_w*g_h);
	//cudaMemcpy(host_disp,g_disparityLeft2,g_w*sizeof(float)*g_h,cudaMemcpyDeviceToHost);

	//cout<< "hayyy"<<endl;
	//printToFile("disparityrl.txt",host_disp,g_h,g_w);
	//exit(1);
	// Now draw the disparity map


	drawDisparityKernel<<<grid,threads>>>(DisparityMap,DispPitch/sizeof(uchar4),g_disparityLeft2,g_floatDispPitch,g_w,g_h);
	cudaThreadSynchronize();
	CUT_CHECK_ERROR("Kernel execution failed writing disparity map!");

	/*cudaMemcpy(disparity_map_temp,DisparityMap,g_w*g_h*4,cudaMemcpyDeviceToHost);

	RGBA_to_RGB((uchar4*)disparity_map_temp, (uchar3*)disparity_map, RGB_pitch, g_w,g_h);
	leftImg_name[0]='\0';


	sprintf(leftImg_name,"disparity_map.ppm");

	imagefile_left = fopen(leftImg_name,"wb");

	//write out left image
	fprintf( imagefile_left, "P6\n%u %u 255\n", g_w, g_h );
	fwrite( (const char *)disparity_map, 1,
			g_w* g_h*3, imagefile_left);
	fclose(imagefile_left);*/

	// now copy the output for openGL
	cudaGLMapBufferObject( (void**)&GLtemp, DisparityImage_GLBufferID);
	cudaMemcpy2D(GLtemp,g_w*sizeof(uchar4),DisparityMap,DispPitch,g_w*sizeof(uchar4),g_h,cudaMemcpyDeviceToDevice);
	cudaGLUnmapBufferObject(DisparityImage_GLBufferID);



	cudaFree(DisparityMap);
	cudaFree(left_grey);
	cudaFree(right_grey);

	/*float3 Luv_image_array[g_w*g_h];
	cudaMemcpy(RGBImage,p_hostLeft,g_w*sizeof(uchar3)*g_h,cudaMemcpyHostToDevice);
	convertRGB_to_MLUV<<<grid,threads>>>(RGBImage,LuvImage,RGB_pitch,Luv_pitch,g_w,g_h);
	cudaThreadSynchronize();


	if(!SOM_TRAINED){
		cudaMemcpy(Luv_image_array,LuvImage,g_w*g_h*sizeof(float3),cudaMemcpyDeviceToHost);

	//	printToFile(final_name,Luv_image_array,g_w,g_h);

		SOM_MAP->loadSOM("SOM.txt");
		//SOM_MAP->cluster_SOM();
		SOM_TRAINED=1;
	}
	//cudaMemcpy(Luv_image_array,LuvImage,g_w*g_h*sizeof(float3),cudaMemcpyDeviceToHost);
	//SOM_MAP->train(Luv_image_array);
	SOM_MAP->segment(LuvImage,Luv_pitch,seg_image);*/



	//cudaMemcpyToArray(g_leftTex_array, 0, 0, RGBImage,g_w * g_h*sizeof(uchar3), cudaMemcpyDeviceToDevice);
	//cudaMemcpyToArray(g_rightTex_array, 0, 0, p_hostRight,g_w * g_h*sizeof(uchar3), cudaMemcpyHostToDevice);

	//CUT_SAFE_CALL(cutStopTimer(timer));

	//float retval = cutGetTimerValue(timer);


	return retval;
}

/**********
CleanupStereo - releses host & GPU memory allocated
***********/
StereoCUDA::~StereoCUDA()
{
	SOM_MAP->~SOM_SA();
	rectifier->~Rectifier();
	//mapper->~StereoMapper();
	lineDetector->~LineDetector();
	cudaFreeArray(g_leftTex_array);
	cudaFreeArray(g_rightTex_array);
	cudaFree(g_disparityLeft);
	cudaFree(g_disparityLeft2);
	cudaFree(g_minSSD);
	cudaFree(LuvImageLeft);
	cudaFree(LuvImageRight);
	cudaFree(LeftImage);
	cudaFree(RightImage);
}
