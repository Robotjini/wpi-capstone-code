/*
 * RectifierHost.cpp
 *
 *  Created on: May 12, 2010
 *      Author: igvcmqp
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;

#include <RectifierHost.h>

RectifierHost::RectifierHost(unsigned int width, unsigned int height){
	g_w = width;
	g_h = height;

	mx_left = (float*)malloc(sizeof(float)*g_w*g_h);
	my_left = (float*)malloc(sizeof(float)*g_w*g_h);
	mx_right = (float*)malloc(sizeof(float)*g_w*g_h);
	my_right = (float*)malloc(sizeof(float)*g_w*g_h);

	loadMaps();
}

RectifierHost::~RectifierHost(){
	free(mx_left);
	free(my_left);
	free(mx_right);
	free(my_right);
}

void RectifierHost::loadMaps(){
	ifstream mx_leftfile, mx_rightfile, my_leftfile, my_rightfile;
	int bufSize=256;
	char buffer[bufSize];

	mx_leftfile.open(MX_LEFT);
	my_leftfile.open(MY_LEFT);
	mx_rightfile.open(MX_RIGHT);
	my_rightfile.open(MY_RIGHT);
	cout<<"loading maps"<<endl;
	for(int i=0;i<g_h;i++){
		for(int j=0;j<g_w;j++){
			//load left map points
			if(j==g_w-1) mx_leftfile.get(buffer,bufSize,'\n');
			else mx_leftfile.get(buffer,bufSize,',');
			mx_left[i*g_w+j] = atof(buffer);
			mx_leftfile.get();

			if(j==g_w-1) my_leftfile.get(buffer,bufSize,'\n');
			else my_leftfile.get(buffer,bufSize,',');
			my_left[i*g_w+j] = atof(buffer);
			my_leftfile.get();

			//load right map points
			if(j==g_w-1) mx_rightfile.get(buffer,bufSize,'\n');
			else mx_rightfile.get(buffer,bufSize,',');
			mx_right[i*g_w+j] = atof(buffer);
			mx_rightfile.get();

			if(j==g_w-1) my_rightfile.get(buffer,bufSize,'\n');
			else my_rightfile.get(buffer,bufSize,',');
			my_right[i*g_w+j] = atof(buffer);
			my_rightfile.get();
		}
	}
	mx_leftfile.close();
	my_leftfile.close();
	mx_rightfile.close();
	my_rightfile.close();

	_mx_left = cvMat(g_h,g_w,CV_32F,mx_left);
	_my_left = cvMat(g_h,g_w,CV_32F,my_left);
	_mx_right = cvMat(g_h,g_w,CV_32F,mx_right);
	_my_right = cvMat(g_h,g_w,CV_32F,my_right);
}

void RectifierHost::rectify(unsigned char *host_left, unsigned char* host_right){
	 IplImage* left = cvCreateImage(cvSize(g_w,g_h),8,3);
	 IplImage* right = cvCreateImage(cvSize(g_w,g_h),8,3);

	 left->imageData = (char*)host_left;
	 right->imageData = (char*)host_right;
	 IplImage* img2r= cvCloneImage( right);
	 IplImage* img1r = cvCloneImage( left);

	 cvRemap( left, img1r, &_mx_left, &_my_left);
     cvRemap( right, img2r, &_mx_right, &_my_right);

     memcpy(host_left, img1r->imageData,g_h*g_w*3);
     memcpy(host_right, img2r->imageData, g_h*g_w*3);
     cvReleaseImage( &left);
     cvReleaseImage( &right);
     cvReleaseImage( &img1r );
     cvReleaseImage( &img2r );
}
