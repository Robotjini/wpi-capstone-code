/*
 * LineDetector.cu
 *
 *  Created on: Apr 14, 2010
 *      Author: igvcmqp
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;

#include <LineDetector.h>

#define MIN_LUM					150 //change for better white line detection
#define MAX_UV					70
#define MIN_UV					-70
#define EQ1_X2					23.275 //equation 1 x^2 var
#define EQ1_X1					-271.3//equation 1 x^1 var
#define EQ1_X0					1214.7//equation 1 x^0 var
#define EQ1_CUTOFF				5.5
#define EQ2_X2					1.1895
#define EQ2_X1					-34.82
#define EQ2_X0					569.25
#define MAP_MAX_X				10.05 //meters
#define MAP_MAX_Y				10.05 //meters
#define MAP_MIN_Y				-10.05//meters
#define MAP_STEP				.1//meters
#define MAP_WIDTH				201
#define MAP_HEIGHT				101
#define WHITE_SEGMENT			216
#define CONE_SEGMENT			144 //based off of visually appealing scale of cluster_val+1 *36
#define PERCENT_THRESHOLD		.05 //percentage of white points in each bin before its considered white line
#define PERCENT_THRESHOLD_CONE	.25
#define CONE_OBJECT				2
#define LINE_OBJECT				255
#define Y_OFFSET				0

LineDetector::LineDetector(int width, int height){
	g_w =width;
	g_h=height;

	cudaMallocPitch((void**)&line_segments, &img_pitch,g_w*sizeof(unsigned char),g_h);
	img_pitch /= sizeof(unsigned char);
	initialize();
}

LineDetector::~LineDetector(){
	cudaFree(line_segments);
	cudaFree(block_limits);
	cudaFree(local_map);
}

void LineDetector::initialize(){
	unsigned char *local_map_host;

	local_map_host=(unsigned char*)malloc(sizeof(unsigned char)*MAP_WIDTH*MAP_HEIGHT);

	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(MAP_WIDTH,threads.x);
	grid.y = divUp(MAP_HEIGHT,threads.y);

	cudaMallocPitch((void**)&block_limits, &map_pitch,MAP_WIDTH*sizeof(int4),MAP_HEIGHT);
	cudaMallocPitch((void**)&local_map,&map_pitch,MAP_WIDTH*sizeof(unsigned char),MAP_HEIGHT);
	map_pitch =MAP_WIDTH;

	float horz_AOV = loadHorizontalAOV();


	initialize_kernel<<<grid,threads>>>(block_limits,local_map, map_pitch,tan(horz_AOV/2), g_w, g_h);
	cudaThreadSynchronize();
	cudaMemcpy(local_map_host,local_map,MAP_WIDTH*sizeof(unsigned char)*MAP_HEIGHT,cudaMemcpyDeviceToHost);
	printToFile("local_map.txt",local_map_host,MAP_WIDTH,MAP_HEIGHT);
	free(local_map_host);
}

void LineDetector::drawBlocks(uchar3 *blocks_img){
	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(MAP_WIDTH,threads.x);
	grid.y = divUp(MAP_HEIGHT,threads.y);

	drawBlocks_kernel<<<grid,threads>>>(blocks_img, block_limits, img_pitch, map_pitch);
	cudaThreadSynchronize();
}

void LineDetector::segmentLines(float3* LuvImage, unsigned char ** line_img){
	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(g_w,threads.x);
	grid.y = divUp(g_h,threads.y);

	segmentLines_kernel<<<grid,threads>>>(LuvImage,line_segments,img_pitch,g_w,g_h);
	cudaThreadSynchronize();

	*line_img = line_segments;
	//cudaMemcpy(line_img,line_segments,g_w*sizeof(unsigned char)*g_h,cudaMemcpyDeviceToHost);
}

//load in the horizontal angle of view of the camera system
//currently done hardcoded but could also use the derived focal length from calibration
float LineDetector::loadHorizontalAOV(){
	return 2*atan(IMAGE_SENSOR_WIDTH/(2*FOCAL_LENGTH));
}

//load in the vertical angle of view of the camera system
//currently done hardcoded but could also use the derived focal length from calibration
float LineDetector::loadVerticalAOV(){
	return 2*atan(IMAGE_SENSOR_HEIGHT/(2*FOCAL_LENGTH));
}

void LineDetector::detectLines(unsigned char *seg_img, unsigned char **ret_img){
	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(MAP_WIDTH,threads.x);
	grid.y = divUp(MAP_HEIGHT,threads.y);

	detectLines_kernel<<<grid,threads>>>(seg_img, block_limits, local_map, img_pitch, map_pitch);
	cudaThreadSynchronize();

	cudaMemcpy(*ret_img,local_map,MAP_WIDTH*MAP_HEIGHT*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	//saveImage("seg_img",2,seg_img,1,201,101);
	//*ret_img = local_map;
}

void LineDetector::detectCones(unsigned char *seg_img){
	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(MAP_WIDTH,threads.x);
	grid.y = divUp(MAP_HEIGHT,threads.y);
	unsigned char *local_map_host;

	local_map_host=(unsigned char*)malloc(sizeof(unsigned char)*MAP_WIDTH*MAP_HEIGHT);

	detectCones_kernel<<<grid,threads>>>(seg_img, block_limits, local_map, img_pitch, map_pitch);
	cudaThreadSynchronize();

	grid.y=1;
	threads.y=1;
	cleanCones_kernel<<<grid,threads>>>(local_map, map_pitch);
	cudaThreadSynchronize();

	cudaMemcpy(local_map_host,local_map,MAP_WIDTH*sizeof(unsigned char)*MAP_HEIGHT,cudaMemcpyDeviceToHost);
	saveImage("seg_img",1,local_map_host,1,201,101);
	printToFile("local_maplines.txt",local_map_host,MAP_WIDTH,MAP_HEIGHT);
	free(local_map_host);
	exit(1);
}

__global__ void detectCones_kernel(unsigned char *seg_img, int4 *block_limits, unsigned char* local_map, size_t img_pitch, size_t map_pitch){
    int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
    int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
    float percent=0;
    float wcount=0;//white point count
    float total=0;

    if(x < MAP_WIDTH && y < MAP_HEIGHT){
    	int4 limits = block_limits[__mul24(y,map_pitch) +x];
    	if(limits.x != -1){
    		for(int i=limits.z; i< limits.w;i++)
    			for(int j=limits.x;j<limits.y;j++){
    				if(seg_img[__mul24(j,img_pitch) +i]==CONE_SEGMENT) wcount++;
    				total++;
    			}
    		percent = wcount/total;
    		if(percent > PERCENT_THRESHOLD_CONE  && local_map[__mul24(y,map_pitch) +x] != LINE_OBJECT )
    			local_map[__mul24(y,map_pitch) +x]=CONE_OBJECT;
    	}
    }
}

//here it is 1 thread per map column so 201 threads
//basically takes only 1st cone block detected and removes rest
__global__ void cleanCones_kernel(unsigned char* local_map, size_t map_pitch){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
    int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
    float cone_detected=0;

    if(x < MAP_WIDTH && y == 0){
    	for(int i=MAP_HEIGHT;i>=0;i--){
    		if(local_map[__mul24(i,map_pitch) +x]==CONE_OBJECT){
    			if(cone_detected) local_map[__mul24(i,map_pitch) +x] =0;
    			else cone_detected=1;
    		}
    	}
    }
}

//takes in full segment image and edits linedetectors local map to reflect lines found
__global__ void detectLines_kernel(unsigned char *seg_img, int4 *block_limits, unsigned char *local_map, size_t img_pitch, size_t map_pitch){
    int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
    int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
    float percent=0;
    float wcount=0;//white point count
    float total=0;

    if(x < MAP_WIDTH && y < MAP_HEIGHT){
    	int4 limits = block_limits[__mul24(y,map_pitch) +x];
    	if(limits.x != -1){
    		for(int i=limits.z; i< limits.w;i++)
    			for(int j=limits.x;j<limits.y;j++){
    				if(seg_img[__mul24(j,img_pitch) +i]==WHITE_SEGMENT) wcount++;
    				total++;
    			}
    		percent = wcount/total;
    		if(percent > PERCENT_THRESHOLD) local_map[__mul24(y,map_pitch) +x]=LINE_OBJECT;
    		else local_map[__mul24(y,map_pitch) +x]=0;
    	}
    }
}

__global__ void drawBlocks_kernel(uchar3 *blocks_img, int4* block_limits, size_t img_pitch, size_t map_pitch){
    int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
    int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

    if(x < MAP_WIDTH && y < MAP_HEIGHT){
    	int4 limits = block_limits[__mul24(y,map_pitch) +x];
    	if(limits.x != -1){
    		uchar3 color;
    		get_color(&color,x);
    		for(int i=limits.z; i< limits.w;i++)
    			for(int j=limits.x;j<limits.y;j++){
    				blocks_img[__mul24(j,img_pitch) +i]=color;
    			}
    	}
    }
}

__device__ void get_color(uchar3* color, int num){
	int temp = num%6;
	switch(temp){
		case 0:
			color->x =255;
			color->y =0;
			color->z =0;
			break;
		case 1:
			color->x =0;
			color->y =255;
			color->z =0;
			break;
		case 2:
			color->x =0;
			color->y =0;
			color->z =255;
			break;
		case 3:
			color->x =255;
			color->y =255;
			color->z =0;
			break;
		case 4:
			color->x =255;
			color->y =0;
			color->z =255;
			break;
		case 5:
			color->x =0;
			color->y =255;
			color->z =255;
			break;
	}
}

__global__ void initialize_kernel(int4* block_limits, unsigned char* local_map, size_t map_pitch, float horz_AOV, int img_width, int img_height){
    int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
    int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

    if(x < MAP_WIDTH && y < MAP_HEIGHT){
    	int4 temp;
    	temp.x=-1;
    	temp.y=-1;
    	temp.z=-1;
    	temp.w=-1;

    	block_limits[__mul24(y,map_pitch) +x] = temp;
    	local_map[__mul24(y,map_pitch) +x] = -1;

    	float top, bot, left, right;
    	top= MAP_MAX_X - (y*MAP_STEP);
    	bot= MAP_MAX_X - ((y+1)*MAP_STEP-.01);
    	left= MAP_MIN_Y + (x*MAP_STEP);
    	right= MAP_MIN_Y + ((x+1)*MAP_STEP-.01);
    	if(left >0) left=left+.01;
    	if(right>0) right=right+.01;

    	int p_top, p_left, p_bot, p_right; //p=pixel
    	p_top = get_pixelX(top);
    	p_bot = get_pixelX(bot);
    	float w_m = 2*top*horz_AOV;
    	p_left = get_pixelY(left,w_m,img_width);
    	w_m = 2*bot*horz_AOV;
    	p_right =get_pixelY(right,w_m,img_width);

    	if(!(p_top >= img_height || p_bot < 0 || p_left < 0 || p_right >= img_width)){
    		temp.x=p_top;
    		temp.y=p_bot;
    		temp.z=p_left;
    		temp.w=p_right;
    		block_limits[__mul24(y,map_pitch) +x] = temp;
    		local_map[__mul24(y,map_pitch) +x]=1;
    	}else{
    		local_map[__mul24(y,map_pitch) +x]=0;
    	}
    }
}

__device__ int get_pixelX(float x_dist){
	if(x_dist < EQ1_CUTOFF){
		return (int)(EQ1_X2*x_dist*x_dist + EQ1_X1*x_dist + EQ1_X0);
	}else{
		return (int)(EQ2_X2*x_dist*x_dist + EQ2_X1*x_dist + EQ2_X0);
	}
}

__device__ int get_pixelY(float y_dist, float w_m, int img_width){
	return (int)(((y_dist+Y_OFFSET+(w_m/2)) *img_width)/w_m);
}

__global__ void segmentLines_kernel(float3* LuvImage, unsigned char* line_segments, size_t img_pitch, int width, int height){
    int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
    int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

    float3 temp;
    if(x < width && y < height){
    	temp = LuvImage[__mul24(y,img_pitch) +x];
    	if(temp.x > MIN_LUM && temp.y < MAX_UV && temp.y > MIN_UV && temp.z < MAX_UV && temp.z > MIN_UV){
    		line_segments[__mul24(y,img_pitch) +x] = 216;
    	}
    	else line_segments[__mul24(y,img_pitch) +x] = 0;
    }
}
