/*
 * StereoMapper.cu
 *
 *  Created on: Apr 3, 2010
 *      Author: igvcmqp
 */
/*
 * NOT IN OFFICIAL USE
 */
#include <StereoCUDA.h>
#include <StereoHelper.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <iostream>
using namespace std;

#define MAP_WIDTH		201 //hardcoded change this!!!! during integration
#define MAP_HEIGHT		100
#define MAP_STEP		10 //10cm step
#define OBJ_THRESH		750

#include <StereoMapper.h>

StereoMapper::StereoMapper(int img_w,int img_h){
	map_w = MAP_WIDTH;
	map_h = MAP_HEIGHT;

	cudaMallocPitch((void**)&xy_map,&map_pitch,img_w*sizeof(float3),img_h);
	cudaMallocPitch((void**)&local_map,&map_pitch,map_w*sizeof(int),map_h);

	map_pitch = MAP_WIDTH;
	cout<<"map_pitch:"<<map_pitch<<endl;
	baseline = loadBaseline();
	focal_len =loadFocalLength();
	horizontal_AOV = loadHorizontalAOV();
	vertical_AOV = loadVerticalAOV();
	cout<<"horiz:"<<horizontal_AOV<<",vert:"<<vertical_AOV<<endl;
}

StereoMapper::~StereoMapper(){
	cudaFree(local_map);
	cudaFree(xy_map);
}

float StereoMapper::loadBaseline(){
	ifstream T;
	int bufSize=256;
	char buffer[bufSize];
	float3 t_m;

	T.open(TRANSLATION_MATRIX);

	if(!T.is_open()){
		cout<<"Fatal error in StereoMapper::getBaseline().  File: "<<TRANSLATION_MATRIX<<" not found."<<endl;
		exit(1);
	}

	T.get(buffer,bufSize,',');
	t_m.x = atof(buffer);
	T.get();
	T.get(buffer,bufSize,',');
	t_m.y = atof(buffer);
	T.get();
	T.get(buffer,bufSize,'\n');
	t_m.z = atof(buffer);

	T.close();

	return (sqrt((t_m.x * t_m.x) + (t_m.y*t_m.y) + (t_m.z * t_m.z))*100)/PIXEL_SIZE;
}
//returns focal length of cameras in pixels
float StereoMapper::loadFocalLength(){
	return FOCAL_LENGTH/PIXEL_SIZE;//temporary should load from file
}

//load in the horizontal angle of view of the camera system
//currently done hardcoded but could also use the derived focal length from calibration
float StereoMapper::loadHorizontalAOV(){
	return 2*atan(IMAGE_SENSOR_WIDTH/(2*FOCAL_LENGTH));
}

//load in the vertical angle of view of the camera system
//currently done hardcoded but could also use the derived focal length from calibration
float StereoMapper::loadVerticalAOV(){
	return 2*atan(IMAGE_SENSOR_HEIGHT/(2*FOCAL_LENGTH));
}

void StereoMapper::map2D(int *dest, float *disp_map, size_t img_pitch, int img_width, int img_height){
	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(img_width,threads.x);
	grid.y = divUp(img_height,threads.y);

	int *local_map_host= (int*)malloc(sizeof(int) * map_w*map_h);
	float3 *host_disp_map = (float3*)malloc(sizeof(float3) * img_width*img_height);
	float *max_depth;
	size_t temp_pitch;
	cudaMallocPitch((void**)&max_depth,&temp_pitch,img_width*sizeof(float),1);

	threads.y =1;
	grid.y=1;
	grid.x = divUp(img_width,threads.x);

	//map2D_kernel<<<grid,threads>>>(disp_map,xy_map,img_pitch,baseline,focal_len,tan(horizontal_AOV/2),tan(vertical_AOV/2),img_width,img_height);
	//cudaThreadSynchronize();


	grid.x = divUp(map_w,threads.x);
	grid.y = divUp(map_h,threads.y);

	//init_local_map<<<grid,threads>>>(local_map, map_pitch);
	//cudaThreadSynchronize();

	threads.x=1;
	threads.y=1;
	grid.x=1;
	grid.y=1;
	//pop_local_map<<<grid,threads>>>(xy_map,local_map,img_pitch, map_pitch,img_width,img_height);
	//cudaThreadSynchronize();

	threads.x=16;
	threads.y=16;
	grid.x = divUp(map_w,threads.x);
	grid.y = divUp(map_h,threads.y);
	//clean_local_map<<<grid,threads>>>(local_map, map_pitch);
	//cudaThreadSynchronize();

	//cudaMemcpy(host_disp_map,xy_map,img_width*sizeof(float3)*img_height,cudaMemcpyDeviceToHost);
	//cudaMemcpy(local_map_host,local_map,map_w*sizeof(int)*map_h,cudaMemcpyDeviceToHost);

	//printToFile("local_map.txt",local_map_host,map_w,map_h);
	//printToFile("disp_map.txt",host_disp_map,img_width, img_height);
	free(local_map_host);
	free(host_disp_map);
}

/*__global__ void convert_to_2D(float *disp_map, float *max_depth, size_t img_pitch, float baseline, float focal_len, float horz_AOV, float vert_AOV, int img_width, int img_height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;

	float temp_disp;
	float max_disp;

	if(x < img_width){
		max_disp = disp_map[x];
		for(int i=1;i<img_height;i++){

			temp_disp = disp_map[__mul24(i,img_pitch)+x];
			if(temp_disp > max_disp) max_disp = temp_disp;
		}

		if(max_disp != STEREO_MIND-1){
			disp = STEREO_MAXD + disp;
			depth = ((baseline *focal_len)/disp) *PIXEL_SIZE;
	}
}*/

__global__ void pop_local_map(float3 *xy_map, int* local_map, size_t img_pitch, size_t map_pitch, int img_width, int img_height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	float3 temp;

	for(int i=0;i<img_height;i++){
		for(int j=0;j<img_width;j++){
			temp = xy_map[__mul24(img_pitch,i) + j];
			if(temp.x !=-1){
				temp.x += MAP_WIDTH/2;
				if (temp.y < MAP_HEIGHT && temp.x < MAP_WIDTH){
					local_map[__mul24(map_pitch,(int)temp.y) + (int)temp.x]++;
				}
			}
		}
	}
}

__global__ void clean_local_map(int* local_map, size_t map_pitch){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

	int temp;

	if((y < MAP_HEIGHT) && (x < MAP_WIDTH)){
		temp= local_map[__mul24(map_pitch,y) + x];
		if(temp<OBJ_THRESH) local_map[__mul24(map_pitch,y) + x]=0;
		else local_map[__mul24(map_pitch,y) + x]=1;
	}
}

__global__ void init_local_map(int* local_map, size_t map_pitch){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

	if((y < MAP_HEIGHT) && (x < MAP_WIDTH)){
		local_map[__mul24(map_pitch,y) + x]=0;
	}
}

//old file possibly not useful
__global__ void map2D_kernel(float *disp_map, float3 *xy_map, size_t img_pitch, float baseline, float focal_len, float horz_AOV, float vert_AOV, int img_width, int img_height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

	float disp, depth,w_cm, h_cm;
	float3 tempxy;

	if((y < MAP_HEIGHT) && (x < MAP_WIDTH)){
		tempxy.x=-1;
		tempxy.y=-1;
		tempxy.z=-1;
		disp = disp_map[__mul24(img_pitch,y) + x];
		if(disp != STEREO_MIND-1){
			disp = STEREO_MAXD + disp;
			depth = ((baseline *focal_len)/disp) *PIXEL_SIZE;
			if(depth > MAP_HEIGHT*MAP_STEP || depth < 0){
				xy_map[__mul24(img_pitch,y) + x] = tempxy;
			}else{

				w_cm = 2*depth*horz_AOV;//horz_AOV already calculated as arctan(horizontalAOV/2)
				h_cm = 2*depth*vert_AOV;//vert_AOV already calculated as arctan(verticalAOV/2)

				tempxy.x = (((w_cm/img_width)*x)-w_cm/2)/10;

				tempxy.y =depth/10;

				tempxy.z = (((h_cm/img_height)*y)-h_cm/2)/10;

				xy_map[__mul24(img_pitch,y) + x] =tempxy;
			}
		}else xy_map[__mul24(img_pitch,y) + x] = tempxy;
	}
}
