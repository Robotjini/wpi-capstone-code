/**
 * @author Christopher Gamache(gamache@wpi.edu)
 * @date 04/08/10 (date commented)
 *
 * @brief This class and source file combines to effectively create and cluster a neural network to perform segmentation.
 * The SOM(Self Organizing Map) that is created is put through a series of training functions to effectively get a set of averages
 * for normal image values in the modified L*u*v* colorspace.  The training for this network is done separately and at the time of this writing
 * there is no command line argument to restart the training. Currently it must be specifically coded in.  However once trained the SOM map
 * can be loaded from file using load_SOM and be separately reclustered or just simply used.  This was done for the simple fact that
 * training the SOM map takes a long time and cannot/should not be done everytime the system starts.
 *
 * The SOM map itself is default 16x16, this seems to work out well and has the added benefit of mapping directly to grey scale.(i.e node indexes range from 0-255)
 * Increasing the map size will increases accuracy with an exponential runtime increase.
 */
#include <SOM_SA.h>
#include <StereoCUDA.h>
#include <StereoHelper.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <iostream>
using namespace std;
#define WINDOW_SIZE	7  //window size for averaging 25 = 51x51 window

/**
 * @brief on creation the SOM_SA class allocates space for the SOM map and other necessary storage blocks on the GPU
 * This is done in an effort to reduce runtime
 * @param width width of the images coming from the stereo vision system
 * @param height height of images coming from stereo vision system
 */
SOM_SA::SOM_SA(int width, int height){
	g_w=width;
	g_h=height;
	cudaMallocPitch((void**)&SOM_map,&SOM_pitch,SOM_MAP_SIZE*sizeof(float4),SOM_MAP_SIZE);
	cudaMallocPitch((void**)&ssd,&ssd_pitch,SOM_MAP_SIZE*sizeof(float),SOM_MAP_SIZE);
	cudaMallocPitch((void**)&clusterImage,&cluster_pitch,g_w,g_h);
	cudaMalloc(&segImage, g_w*g_h*sizeof(unsigned char));

	SOM_pitch = SOM_pitch/sizeof(float4);
	ssd_pitch = ssd_pitch/sizeof(float);

	//cudaChannelFormatDesc float4Tex = cudaCreateChannelDesc<float4>();
	//cudaMallocArray(&SOM_Tex, &float4Te``x, SOM_MAP_SIZE, SOM_MAP_SIZE);

	print_GPU_mem();
}

/**
 * @brief frees GPU structures allocated on class creation
 */
SOM_SA::~SOM_SA(){
	cudaFree(SOM_map);
	cudaFree(ssd);
	cudaFree(clusterImage);
	cudaFree(segImage);
}

/**
 * @brief initializes SOM map values using smart random values that mimic the actual value ranges of Modified L*u*v* colorspace
 * @param random_array the array to be initialized
 */
__host__ void SOM_SA::initialize_SOM(float4 *random_array){
	float4 temp_val;
	for(int i=0;i < SOM_MAP_SIZE*SOM_MAP_SIZE;i++){
			temp_val = random_array[i];
			temp_val.x = (float) 160*((float)rand()/RAND_MAX);
			temp_val.y = (float) ((120*(float)rand()/RAND_MAX)-60);
			temp_val.z = (float) ((120*(float)rand()/RAND_MAX)-60);
			temp_val.w = rand()%NUM_CLUSTERS;
			random_array[i] = temp_val;
	}
}
/**
 * @brief The function train performs a single training sequence on the input image, with a radius of 1 and a learning rate of .0005
 * This function is meant to be used after the SOM map has already gone through an initial training sequence done in the function create_SOM.
 * @param LuvImage image to be used for training
 * @warning Prints out the file SOM_map to file after training.  If there is no backup changes are perminate.
 * The file name used is defined by the #define called SOM_FILENAME, this can be changed in the header file.
 *
 */
void SOM_SA::train(float3 *LuvImage){
	float4 tempSOM[SOM_MAP_SIZE][SOM_MAP_SIZE];
	train_SOM(SOM_map, ssd, LuvImage, SOM_pitch, ssd_pitch, 1, .0005f);
	cudaMemcpy(tempSOM,SOM_map,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float4),cudaMemcpyDeviceToHost);
	printToFile(SOM_FILENAME,(float4*)tempSOM,SOM_MAP_SIZE,SOM_MAP_SIZE);
}

/**
 * @brief loads SOM map from file given the filename
 * @param file_name name of the file containing the SOM map
 * @warning NO ERROR CHECKING.  If the file is incomplete,formatted wrong or does not exist this will crash the program with a segfault.
 */
void SOM_SA::loadSOM(char *file_name){
	ifstream inFile;
	int bufSize=SOM_MAP_SIZE*2;
	char buffer[bufSize];
	float4 tempSOM[SOM_MAP_SIZE][SOM_MAP_SIZE];

	inFile.open(file_name);

	for(int i=0;i<SOM_MAP_SIZE;i++){
		for(int j=0;j<SOM_MAP_SIZE;j++){
			inFile.get(buffer,bufSize,',');
			tempSOM[i][j].x = atof(buffer);
			inFile.get();
			inFile.get(buffer,bufSize,',');
			tempSOM[i][j].y = atof(buffer);
			inFile.get();
			inFile.get(buffer,bufSize,',');
			tempSOM[i][j].z = atof(buffer);
			inFile.get();
			if(j==SOM_MAP_SIZE-1) inFile.get(buffer,bufSize,'\n');
			else inFile.get(buffer,bufSize,',');
			tempSOM[i][j].w = atof(buffer);
			inFile.get();
		}
	}
	inFile.close();
	cudaMemcpy(SOM_map,tempSOM,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float4),cudaMemcpyHostToDevice);
}

/**
 * @brief Creates a new 16x16 SOM map and trains it three times on the given L*u*v* image.
 * This function is meant to be used once only to setup the initial SOM for further training.
 * After training the SOM map is clustered and written out to the file SOM.txt.
 * @param LuvImage Input modified L*u*v* image in float3 format
 */
__host__ void SOM_SA::create_SOM(float3 *LuvImage){
	float4 random_array[SOM_MAP_SIZE*SOM_MAP_SIZE];

	initialize_SOM(random_array);

	cudaMemcpy(SOM_map,random_array,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float4),cudaMemcpyHostToDevice);

	//run first training sequence radius = 16 learn rate = .05
	train_SOM(SOM_map, ssd, LuvImage, SOM_pitch, ssd_pitch, 16, .04f);
	//cudaMemcpy(random_array,SOM_map,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float3),cudaMemcpyDeviceToHost);

	//run second training sequence radius = 5 learn rate = .02
	train_SOM(SOM_map, ssd, LuvImage, SOM_pitch, ssd_pitch, 5, .015f);

	train_SOM(SOM_map, ssd, LuvImage, SOM_pitch, ssd_pitch, 1, .0075f);
	cluster_SOM();
	cudaMemcpy(random_array,SOM_map,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float4),cudaMemcpyDeviceToHost);
	printToFile(SOM_FILENAME,random_array,SOM_MAP_SIZE,SOM_MAP_SIZE);
	//cudaMemcpyToArray(SOM_Tex, 0, 0, SOM_map,SOM_MAP_SIZE * SOM_MAP_SIZE*sizeof(float4), cudaMemcpyDeviceToDevice);
}

__host__ void SOM_SA::print(int counter){
	float4 random_array[SOM_MAP_SIZE*SOM_MAP_SIZE];
	char name[255];
	name[0] = '\0';
	sprintf(name,"/home/igvcmqp/Desktop/TestPics/Somtest%d.txt",counter);
	cudaMemcpy(random_array,SOM_map,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float4),cudaMemcpyDeviceToHost);
	printToFile(name,random_array,SOM_MAP_SIZE,SOM_MAP_SIZE);
}

__host__ void SOM_SA::create_SOM_paper_test(float3 *LuvImage){
	float4 random_array[SOM_MAP_SIZE*SOM_MAP_SIZE];
	initialize_SOM(random_array);

	cudaMemcpy(SOM_map,random_array,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float4),cudaMemcpyHostToDevice);
	//run first training sequence radius = 16 learn rate = .05
	train_SOM(SOM_map, ssd, LuvImage, SOM_pitch, ssd_pitch, 4, .05f);
	//cudaMemcpy(random_array,SOM_map,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float3),cudaMemcpyDeviceToHost);

	//run second training sequence radius = 5 learn rate = .02
	train_SOM(SOM_map, ssd, LuvImage, SOM_pitch, ssd_pitch, 1.25, .02f);

	cluster_SOM();
	cudaMemcpy(random_array,SOM_map,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float4),cudaMemcpyDeviceToHost);
	printToFile(SOM_FILENAME,random_array,SOM_MAP_SIZE,SOM_MAP_SIZE);
	//cudaMemcpyToArray(SOM_Tex, 0, 0, SOM_map,SOM_MAP_SIZE * SOM_MAP_SIZE*sizeof(float4), cudaMemcpyDeviceToDevice);
}

/**
 * @brief Trains the SOM map on every pixel of the input L*u*v* image based on learning rate and neighborhood radius.
 * For fine tune training a radius of 2 or 1 is best with a learning rate at or below .005.  For initial training on the 16x16 SOM
 * a radius of 16 and learn rate of about .05 is better.
 * @param SOM_map  SOM map to be trained
 * @param ssd GPU allocated space to hold sum squared difference results for each training iteration
 * @param LuvImage The modified L*u*v* image to use for training
 * @param SOM_pitch SOM map pitch i.e for a 16x16 map the pitch is 16
 * @param ssd_pitch pitch of the ssd structure, this is the same as the som pitch in this case...redundancy:)
 * @param radius_init The initial neighborhood radius to propagate changes over
 * @param learn_rate_init The initial learning rate, This effects how fast SOM nodes converge upon pixel values.
 * In practice you do not want this overly high since we are more looking for averages and less to match indivdual pixels.
 */
void SOM_SA::train_SOM(float4 *SOM_map, float* ssd, float3* LuvImage, size_t SOM_pitch, size_t ssd_pitch, float radius_init, float learn_rate_init){

	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(SOM_MAP_SIZE,threads.x);
	grid.y = divUp(SOM_MAP_SIZE,threads.y);  // each thread does 1 nodes

	float3 Luv_pixel;
	float ssd_array[SOM_MAP_SIZE*SOM_MAP_SIZE];
	int min_index;
	float4 xyrlr_host, *xyrlr_dev;
	float3 *input_luv_pixel;
	int T = g_w *g_h;
	float radius = radius_init;
	float learn_rate = learn_rate_init;

	cudaMalloc(&input_luv_pixel,sizeof(float3));
	cudaMalloc(&xyrlr_dev,sizeof(float4));


	for (int i =0; i < T; i++){
			Luv_pixel = LuvImage[i];
			cudaMemcpy(input_luv_pixel,&Luv_pixel,sizeof(float3),cudaMemcpyHostToDevice);
			calculate_ssd<<<grid,threads>>>(SOM_map, ssd, input_luv_pixel, SOM_pitch, ssd_pitch);
			cudaThreadSynchronize();

			cudaMemcpy(ssd_array,ssd,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float),cudaMemcpyDeviceToHost);

			min_index = get_min_ssd(ssd_array);
			xyrlr_host.x = min_index % SOM_MAP_SIZE;
			xyrlr_host.y = min_index / SOM_MAP_SIZE;
			xyrlr_host.z = radius;
			xyrlr_host.w = learn_rate;
			cudaMemcpy(xyrlr_dev,&xyrlr_host,sizeof(float4),cudaMemcpyHostToDevice);

			update_weights<<<grid,threads>>>(SOM_map, input_luv_pixel, SOM_pitch, xyrlr_dev);
			cudaThreadSynchronize();

			learn_rate = learn_rate_init* (1 - (i+1)/T);
			radius = radius_init *(2-(i+1)/T);

			//if (i%g_w==0) cout<<i<<endl;
	}
	cudaFree(input_luv_pixel);
	cudaFree(xyrlr_dev);
}

/**
 * @brief given an array of calculated ssd's(sum squared difference) finds and returns the index of the smallest value.
 * @param ssd array containing ssd's between all SOM nodes and a given pixel
 * @return index of lowest value
 */
__host__ int SOM_SA::get_min_ssd(float *ssd){
	int min_index=0;

	for (int i =0; i<SOM_MAP_SIZE*SOM_MAP_SIZE;i++){
		if (ssd[min_index] > ssd[i]){
			min_index=i;
		}
	}

	return min_index;
}

/**
 * @brief Uses simulated annealing to cluster the SOM into color clusters.
 * The number of cluster is determined by the #define NUM_clusters and is defined in the header file.
 * In practice 4-7 clusters seems to be best.
 * Various parameters in cluster_SOM effect the performance of the clustering algorithm.
 * Namely T(temperature), Tmin(min Temperature) and alpha determine how many clustering phases the program executes
 * A clustering phase consists of randomly redistributing NT nodes.(NT,T,Tmin and alpha must all be changed in function)
 * @warning Function performance reliant of hardcoded values defined at start of the function..see description.
 */
void SOM_SA::cluster_SOM(){
	//setup threads
	dim3 grid(1,1,1);
	dim3 threads(NUM_CLUSTERS,1,1);
	float T=3000.0f;
	float Tmin=15.0f;
	float alpha = .99f;
	int NT=512;

	float energy_array_temp[NUM_CLUSTERS];
	float rand_array_host[SOM_MAP_SIZE*SOM_MAP_SIZE];
	float *rand_array_dev;
	float *energy_array;
	float3 cluster_centers[NUM_CLUSTERS];
	cudaError_t error;
	float3 *clusters;
	float energy;
	float4 rand_host, *rand_dev;


	cudaMalloc(&clusters, NUM_CLUSTERS*sizeof(float3));
	cudaMalloc(&rand_dev, sizeof(float4));
	cudaMalloc(&rand_array_dev, SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float4));
	cudaMalloc(&energy_array, NUM_CLUSTERS*sizeof(float));

	for(int i =0;i<SOM_MAP_SIZE*SOM_MAP_SIZE;i++){
		rand_array_host[i] = rand()%NUM_CLUSTERS;
	}
	threads.x=SOM_MAP_SIZE*SOM_MAP_SIZE;
	cudaMemcpy(rand_array_dev,rand_array_host,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float),cudaMemcpyHostToDevice);
	initialize_clusters<<<grid,threads>>>(this->SOM_map,rand_array_dev);
	while(Tmin < T){
		threads.x = NUM_CLUSTERS;
		threads.y = 1;
		compute_cluster_centers<<<grid,threads>>>(this->SOM_map,clusters);
		cudaThreadSynchronize();

		for(int j=0;j < NT;j++){
			threads.x = 1;
			threads.y = 1;
			rand_host.x = rand()%NUM_CLUSTERS;
			rand_host.y =j% (SOM_MAP_SIZE*SOM_MAP_SIZE);//rand()%(SOM_MAP_SIZE*SOM_MAP_SIZE);
			rand_host.z = rand()%1000;
			rand_host.w = T;
			cudaMemcpy(rand_dev,&rand_host,sizeof(float4),cudaMemcpyHostToDevice);
			adjust_nodes<<<grid,threads>>>(rand_dev, this->SOM_map, clusters, SOM_pitch);
			cudaThreadSynchronize();
		}
		threads.x = NUM_CLUSTERS;
		threads.y = 1;
		compute_energy<<<grid,threads>>>(&energy,energy_array,this->SOM_map,clusters);
		cudaThreadSynchronize();

		cudaMemcpy(energy_array_temp,energy_array,sizeof(float)*NUM_CLUSTERS,cudaMemcpyDeviceToHost);
		energy=0;
		for(int i =0;i<NUM_CLUSTERS;i++){
			energy+=energy_array_temp[i];
		}
		cout<<"energy"<<":"<<energy<<endl;
		T=alpha*T;
	}
	float4 tempSOM[SOM_MAP_SIZE][SOM_MAP_SIZE];
	cudaMemcpy(tempSOM,SOM_map,SOM_MAP_SIZE*SOM_MAP_SIZE*sizeof(float4),cudaMemcpyDeviceToHost);
	printToFile(SOM_FILENAME,(float4*)tempSOM,SOM_MAP_SIZE,SOM_MAP_SIZE);
	cudaFree(clusters);
	cudaFree(energy_array);
	cudaFree(rand_dev);
	cudaFree(rand_array_dev);
}

/**
 * @brief Converts the left and right images in modified L*u*v* space into grey scale using the trained SOM map.
 * This function is really used for testing since in practice only one image is converted to L*u*v and the actual grey scale map is never produced.
 */
void SOM_SA::convertToGreyScale(float3 *LuvImage_left, float3* LuvImage_right, size_t Luv_pitch, unsigned char *dest_left, unsigned char *dest_right){
	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(g_w,threads.x);
	grid.y = divUp(g_h,threads.y);

	convertToGreyScale_kernel<<<grid,threads>>>(dest_left,LuvImage_left,SOM_map, cluster_pitch,Luv_pitch, SOM_pitch,g_w, g_h);
	convertToGreyScale_kernel<<<grid,threads>>>(dest_right,LuvImage_right,SOM_map, cluster_pitch,Luv_pitch, SOM_pitch,g_w, g_h);
}

/**
 * @brief segments the given L*u*v* image using trained and clustered SOM map.  The process goes as follows:
 * First the image is reduced to color clusters.  The resulting cluster image goes through one round of k nearest neighbors smoothing.
 * More k nearest neighbors clustering can be added to this sequence however one should be enough.  Additionally you
 * can uncomment the segment_image function, this will put the segmented image into a viewable form.(however it is currently hardcoded for 7 clusters)
 * @param LuvImage image to be segmented in L*u*v* colorspace
 * @param Luv_pitch pitch of the image
 * @param seg_image destination image stored in grey scale
 */
void SOM_SA::segment(float3 *LuvImage, size_t Luv_pitch, unsigned char **seg_image){

	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(g_w,threads.x);
	grid.y = divUp(g_h,threads.y);

	SOM_colorReduction<<<grid,threads>>>(clusterImage,LuvImage,SOM_map, cluster_pitch,Luv_pitch, SOM_pitch,g_w, g_h);
	cudaThreadSynchronize();


	//K_neighbors<<<grid,threads>>>(clusterImage,segImage, cluster_pitch, g_w, g_h);
	//cudaThreadSynchronize();

	segment_image<<<grid,threads>>>(clusterImage, segImage,cluster_pitch, g_w, g_h);
	cudaThreadSynchronize();

	*seg_image = segImage;
	//cudaMemcpy(seg_image,segImage,g_w*g_h,cudaMemcpyDeviceToHost);

}

/**
 * @brief Calculates a disparity average within a given radius for each pixel.  Pixels inside the same segment are counted
 * with significantly weight than those outside.
 * @param disp_map The already calculated disparity map
 * @param return_seg_img Used to return a pointer to the segmented image.  This is not necessary and used most for testing
 * @warning disp_map will be perminately changed after this operation.
 */
void SOM_SA::segmentBasedAverage(float *disp_map, unsigned char **return_seg_img){
	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(g_w,threads.x);
	grid.y = divUp(g_h,threads.y);

	segmentBasedAverage_kernel<<<grid,threads>>>(disp_map,segImage,cluster_pitch,g_w, g_h);
	cudaThreadSynchronize();

	*return_seg_img = segImage;
}

/**
 * @brief GPU kernel program to average the given pixel value based upon the window size and segmented image.
 * WINDOW_SIZE is a #define and can be changed in the header a value of 5 results in a 11x11 averaging area.
 * changing alpha at the start of the function changes how much other segments count towards the average .01 works well
 * it is recommended to stay below .05.  The closer you get to one the more you are just performing averaging and not
 * segmentation based averaging.
 * @param disp_map pointer to disparity map located in GPU memory
 * @param seg_image pointer to segmented image located in GPU memory
 * @param img_step pitch of the segmented image and dispairty map
 * @param width Width of the image
 * @param height Height of the image
 * @warning Perminatley modifies disp_map. Must be called with one thread per pixel.
 */
__global__ void segmentBasedAverage_kernel(float *disp_map, unsigned char *seg_image, size_t img_step, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	float count=0;
	float sum=0;
	float temp_disp;
	float alpha=.01;
	unsigned char cur_seg, pnt_seg;

	if((y < height) && (x < width)){
		if (disp_map[__mul24(y,img_step)+x]==STEREO_MIND-1) return;
		int start_index_y = y -WINDOW_SIZE;
		int end_index_y= y +WINDOW_SIZE;
		int start_index_x= x-WINDOW_SIZE;
		int end_index_x= x+WINDOW_SIZE;

		if(start_index_y < 0) start_index_y=0;
		if(start_index_x < 0) start_index_x=0;
		if(end_index_y >height) end_index_y=height;
		if(end_index_x >width) end_index_x=width;
		pnt_seg= seg_image[__mul24(y,img_step)+x];

		for(int i=start_index_y;i<end_index_y;i++){
			for(int j=start_index_x;j<end_index_x;j++){
				temp_disp = disp_map[__mul24(i,img_step)+j];
				if(temp_disp != STEREO_MIND-1){
					cur_seg = seg_image[__mul24(i,img_step)+j];
					if(pnt_seg == cur_seg){
						sum += temp_disp;
						count += 1;
					}else{
						sum += temp_disp * alpha;
						count += 1*alpha;
					}
				}
			}
		}
		__syncthreads();
		if(count !=0) disp_map[__mul24(y,img_step)+x] = sum/count;
	}
}

/**
 * @brief GPU kernel to convert a Modified L*u*v* image into grey scale based upon SOM map index
 * This only works for a 16x16 SOM map.
 */
__global__ void convertToGreyScale_kernel(unsigned char *destImage,float3* LuvImage, float4* SOM_map, size_t Gray_pitch, size_t Luv_pitch, size_t SOM_pitch, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	float3 temp_Luv;
	float4 temp_SOM;
	float cur_ssd, min_ssd;
	int min_index=0,index;

	if((y < height) && (x < width)){
		temp_Luv = LuvImage[__mul24((height-y-1),Luv_pitch) + x];

		temp_SOM = SOM_map[0];
		min_ssd = __mul24((temp_Luv.x - temp_SOM.x),(temp_Luv.x - temp_SOM.x)) + __mul24((temp_Luv.y - temp_SOM.y),(temp_Luv.y - temp_SOM.y)) + __mul24((temp_Luv.z - temp_SOM.z),(temp_Luv.z - temp_SOM.z));
		for(int i=0;i<SOM_MAP_SIZE;i++){
			for(int j=0;j<SOM_MAP_SIZE;j++){
				index =__mul24(i,SOM_MAP_SIZE) + j;
				temp_SOM = SOM_map[index];
				cur_ssd = __mul24((temp_Luv.x - temp_SOM.x),(temp_Luv.x - temp_SOM.x)) + __mul24((temp_Luv.y - temp_SOM.y),(temp_Luv.y - temp_SOM.y)) + __mul24((temp_Luv.z - temp_SOM.z),(temp_Luv.z - temp_SOM.z));
				if(min_ssd > cur_ssd){
					min_ssd = cur_ssd;
					min_index = index;
				}
			}
		}
		destImage[__mul24((height-y-1),Gray_pitch) + x] = min_index;
	}
}

/**
 * @brief GPU program to initialize SOM map each thread initializes one SOM map node.
 */
__global__ void initialize_clusters(float4 *SOM_map,float *rand){
	int index = threadIdx.x;

	if(index < (SOM_MAP_SIZE*SOM_MAP_SIZE)){
		SOM_map[index].w=rand[index];
	}
}

/**
 * @brief updates the weights of each SOM map node
 * @param xyrlr_dev Holds the x and y index of the winning node for this iteration, along with the current radius and learning rate in that order
 * @warning must have one thread per SOM map node
 */
__global__ void update_weights(float4 *SOM_map, float3 *Luv_pixel, size_t SOM_pitch, float4 *xyrlr_dev){

	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	float4 SOM_val;
	float h_ci=1;
	float h=xyrlr_dev->x;
	float k=xyrlr_dev->y;
	float radius=xyrlr_dev->z;
	float learn_rate=xyrlr_dev->w;

	//perform update if the thread is executing within som map and update radius
	if((y < SOM_MAP_SIZE) && (x < SOM_MAP_SIZE) && (((x-h)*(x-h))+((y-k)*(y-k)) <= (radius*radius))){
		SOM_val = SOM_map[__mul24((SOM_MAP_SIZE-y-1),SOM_pitch) + x];
		get_guassian(&h_ci,1,(float)x,(float)y,h,k,radius,radius);
		SOM_val.x += learn_rate * h_ci *(Luv_pixel->x - SOM_val.x);
		SOM_val.y += learn_rate * h_ci *(Luv_pixel->y - SOM_val.y);
		SOM_val.z += learn_rate * h_ci *(Luv_pixel->z - SOM_val.z);
		SOM_map[__mul24((SOM_MAP_SIZE-y-1),SOM_pitch) + x] = SOM_val;
	}
}
/**
 * @brief Calculates and returns the result of the guassian function for a specific radius, center, and point.
 * @param h_ci container to hold the result of the operation
 * @param A the multiplicative factor or amplitude in most cases should be one.
 * @param x current x point
 * @param y current y point
 * @param h center x point
 * @param k center y point
 * @param spread_x radius of guassian function in the x direction
 * @param spread_y radius of guassian function in the y direction
 * @warning do not change m_e it is the value of natural log, there is no defined constant for this in cuda math
 */
__device__ void get_guassian(float *h_ci ,float A, float x, float y, float h, float k, float spread_x, float spread_y){
	float m_e = 2.71828183;
	*h_ci = A*__powf(m_e,-(((x-h)*(x-h))/(2*(spread_x*spread_x)) + ((y-k)*(y-k))/(2*(spread_y*spread_y))));
}

/**
 * @brief calculates the sum squared difference from each SOM node to the given input pixel and stores the results in the ssd array.
 * @warning must spawn one thread per SOM node.
 */
__global__ void calculate_ssd(float4 * SOM_map, float *ssd, float3 *Luv_pixel, size_t SOM_pitch, size_t ssd_pitch){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	float4 SOM_val;
	float ssd_val;

	if(y < SOM_MAP_SIZE && x < SOM_MAP_SIZE){
		SOM_val = SOM_map[__mul24((SOM_MAP_SIZE-y-1),SOM_pitch) + x];
		ssd_val = (Luv_pixel->x - SOM_val.x)*(Luv_pixel->x - SOM_val.x) + (Luv_pixel->y - SOM_val.y)*(Luv_pixel->y - SOM_val.y) + (Luv_pixel->z - SOM_val.z)*(Luv_pixel->z - SOM_val.z);
		if(SOM_val.x!=SOM_val.x) ssd[__mul24((SOM_MAP_SIZE-y-1),ssd_pitch) + x] = -1;//test for not a number
		else ssd[__mul24((SOM_MAP_SIZE-y-1),ssd_pitch) + x] = ssd_val;
	}
}

__global__ void SOM_colorReduction(unsigned char *clusterImage,float3* LuvImage, float4* SOM_map, size_t Gray_pitch, size_t Luv_pitch, size_t SOM_pitch, int width, int height){

	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	float3 temp_Luv;
	float4 temp_SOM;
	float cur_ssd, min_ssd;
	int min_index=0,index;

	if((y < height) && (x < width)){
		temp_Luv = LuvImage[__mul24((height-y-1),Luv_pitch) + x];

		temp_SOM = SOM_map[0];
		min_ssd = __mul24((temp_Luv.x - temp_SOM.x),(temp_Luv.x - temp_SOM.x)) + __mul24((temp_Luv.y - temp_SOM.y),(temp_Luv.y - temp_SOM.y)) + __mul24((temp_Luv.z - temp_SOM.z),(temp_Luv.z - temp_SOM.z));
		for(int i=0;i<SOM_MAP_SIZE;i++){
			for(int j=0;j<SOM_MAP_SIZE;j++){
				index =__mul24(i,SOM_MAP_SIZE) + j;
				temp_SOM = SOM_map[index];
				cur_ssd = __mul24((temp_Luv.x - temp_SOM.x),(temp_Luv.x - temp_SOM.x)) + __mul24((temp_Luv.y - temp_SOM.y),(temp_Luv.y - temp_SOM.y)) + __mul24((temp_Luv.z - temp_SOM.z),(temp_Luv.z - temp_SOM.z));
				if(min_ssd > cur_ssd){
					min_ssd = cur_ssd;
					min_index = index;
				}
			}
		}
		clusterImage[__mul24((height-y-1),Gray_pitch) + x] = (unsigned char)SOM_map[min_index].w;
	}
}

__global__ void compute_cluster_centers(float4* SOM_map, float3* clusters){
	int index = threadIdx.x;
	float3 cluster_val;
	int node_count=0;
	float4 temp_val;

	if (index < NUM_CLUSTERS){
		cluster_val.x=0;
		cluster_val.y=0;
		cluster_val.z=0;
		for(int j =0;j<SOM_MAP_SIZE*SOM_MAP_SIZE;j++){
			if((int)SOM_map[j].w == index){
				temp_val = SOM_map[j];
				cluster_val.x += temp_val.x;
				cluster_val.y += temp_val.y;
				cluster_val.z += temp_val.z;
				node_count++;
			}

		}
		cluster_val.x = cluster_val.x/node_count;
		cluster_val.y = cluster_val.y/node_count;
		cluster_val.z = cluster_val.z/node_count;
		clusters[index] = cluster_val;
	}
}

__global__ void adjust_nodes(float4 *rand, float4 *SOM_map, float3 *clusters, size_t SOM_pitch){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	float J, J_prime;
	int min_index=0;
	float3 cluster_val;
	float4 SOM_val;
	float m_e = 2.71828183;

	if(x == 0 ){
		cluster_val = clusters[(int)SOM_map[(int)rand->y].w];
		SOM_val= SOM_map[(int)rand->y];
		J = ((cluster_val.x - SOM_val.x)*(cluster_val.x - SOM_val.x))/2 + ((cluster_val.y - SOM_val.y)*(cluster_val.y - SOM_val.y))/2 + ((cluster_val.z - SOM_val.z)*(cluster_val.z - SOM_val.z))/2;
		cluster_val = clusters[(int)rand->x];
		J_prime=((cluster_val.x - SOM_val.x)*(cluster_val.x - SOM_val.x))/2 + ((cluster_val.y - SOM_val.y)*(cluster_val.y - SOM_val.y))/2 + ((cluster_val.z - SOM_val.z)*(cluster_val.z - SOM_val.z))/2;
		if(J_prime < J){
			SOM_map[(int)rand->y].w = rand->x;
		}else if(1000* __powf(m_e,-((J_prime-J)/rand->w)) > rand->z){
			SOM_map[(int)rand->y].w = rand->x;
		}
	}
}

__global__ void compute_energy(float* return_energy, float *energy, float4* SOM_map, float3* clusters){
	int x = threadIdx.x;
	float4 SOM_val;
	float3 cluster_val;
	if(x < NUM_CLUSTERS){
		cluster_val = clusters[x];
		energy[x]=0;
		for( int i=0;i < SOM_MAP_SIZE*SOM_MAP_SIZE;i++){
			if((int)SOM_map[i].w == x){
				SOM_val = SOM_map[i];
				energy[x] += ((cluster_val.x - SOM_val.x)*(cluster_val.x - SOM_val.x))/2 + ((cluster_val.y - SOM_val.y)*(cluster_val.y - SOM_val.y))/2 + ((cluster_val.z - SOM_val.z)*(cluster_val.z - SOM_val.z))/2;
			}
		}
	}
}

__global__ void segment_image(unsigned char* clusterImage, unsigned char* seg_image, size_t cluster_pitch, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	unsigned char cluster_val=0;
	int color=255;
	int x_1 = x+1;
	int y_1 = y+1;

	if(x < width && y < height){
		cluster_val = clusterImage[__mul24(y,cluster_pitch)+x];
		if (x_1 < width){
			if (cluster_val != clusterImage[__mul24(y,cluster_pitch)+x_1]){
				color=0;
			}
		}
		if (y_1 < height){
			if(cluster_val != clusterImage[__mul24(y_1,cluster_pitch)+x]){
				color=0;
			}
		}
		//cluster 5 = white
		//clustre 3=cone
		seg_image[__mul24(y,cluster_pitch)+x]= (cluster_val+1)*36;
	}
}

__global__ void K_neighbors(unsigned char* clusterImage,unsigned char* segImage, size_t cluster_pitch, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

	float count=0;
	float h_ci;
	int radius=1;
	uchar2 kn[NUM_CLUSTERS];
	unsigned char cluster_val='0';
	if(x < width && y < height){
		//clusterImage[__mul24(y,cluster_pitch)+x] = 5;
		for(int i=0;i<NUM_CLUSTERS;i++){
			kn[i].x = i;
			kn[i].y = 0;
		}

		for(int i=radius*-1;i<=radius;i++){
			for(int j=radius*-1;j<=radius;j++){
				if((x+i < width) && (y+j < height) && (y+j >= 0) && (x+i >= 0)){
					cluster_val = clusterImage[__mul24(y+j,cluster_pitch)+x+i];
					kn[(int)cluster_val].y++;
				}
			}
		}

		__syncthreads();
		int max_index;
		unsigned char max;
		max_index=0;
		max = kn[0].y;
		for(int i=0;i<NUM_CLUSTERS;i++){
			if (max < kn[i].y){
				max=kn[i].y;
				max_index=i;
			}
		}
		cluster_val = clusterImage[__mul24(y,cluster_pitch)+x];
		if(kn[cluster_val].y != kn[max_index].y){
			cluster_val =kn[max_index].x;
		}
		clusterImage[__mul24(y,cluster_pitch)+x] = cluster_val;
	}
}


