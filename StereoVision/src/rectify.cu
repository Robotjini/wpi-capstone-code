/*
 * rectify.cu
 *
 *  Created on: Mar 26, 2010
 *      Author: igvcmqp
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;

#include <rectify.h>
#include <StereoHelper.h>
#define COMBINED_LINE_SPACING		50

Rectifier::Rectifier(unsigned int width, unsigned int height){
	g_w = width;
	g_h = height;

	cudaMallocPitch((void**)&temp_left_img,&img_pitch,g_w*sizeof(uchar3),g_h);
	cudaMallocPitch((void**)&temp_right_img,&img_pitch,g_w*sizeof(uchar3),g_h);
	cudaMallocPitch((void**)&left_map,&img_pitch,g_w*sizeof(float2),g_h);
	cudaMallocPitch((void**)&right_map,&img_pitch,g_w*sizeof(float2),g_h);
	img_pitch = img_pitch /sizeof(float2);
	loadMaps();
}

Rectifier::~Rectifier(){
	cudaFree(left_map);
	cudaFree(right_map);
	cudaFree(temp_left_img);
	cudaFree(temp_right_img);
}

void Rectifier::loadMaps(){
	ifstream mx_left, mx_right, my_left, my_right;
	int bufSize=256;
	char buffer[bufSize];
	float2 *temp_left;
	float2 *temp_right;

	temp_left = (float2*)malloc(sizeof(float2)*g_w*g_h);
	temp_right = (float2*)malloc(sizeof(float2)*g_w*g_h);


	mx_left.open(MX_LEFT);
	my_left.open(MY_LEFT);
	mx_right.open(MX_RIGHT);
	my_right.open(MY_RIGHT);

	for(int i=0;i<g_h;i++){
		for(int j=0;j<g_w;j++){
			//load left map points
			if(j==g_w-1) mx_left.get(buffer,bufSize,'\n');
			else mx_left.get(buffer,bufSize,',');
			temp_left[i*g_w+j].x = atof(buffer);
			mx_left.get();

			if(j==g_w-1) my_left.get(buffer,bufSize,'\n');
			else my_left.get(buffer,bufSize,',');
			temp_left[i*g_w+j].y = atof(buffer);
			my_left.get();

			//load right map points
			if(j==g_w-1) mx_right.get(buffer,bufSize,'\n');
			else mx_right.get(buffer,bufSize,',');
			temp_right[i*g_w+j].x = atof(buffer);
			mx_right.get();

			if(j==g_w-1) my_right.get(buffer,bufSize,'\n');
			else my_right.get(buffer,bufSize,',');
			temp_right[i*g_w+j].y = atof(buffer);
			my_right.get();
		}
	}
	mx_left.close();
	my_left.close();
	mx_right.close();
	my_right.close();

	cudaMemcpy(left_map,temp_left,g_w*g_h*sizeof(float2),cudaMemcpyHostToDevice);
	cudaMemcpy(right_map,temp_right,g_w*g_h*sizeof(float2),cudaMemcpyHostToDevice);

	free(temp_left);
	free(temp_right);
}

void Rectifier::rectify(uchar3 *host_left, uchar3* host_right, uchar3 *left_img, uchar3 *right_img){

	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(g_w,threads.x);
	grid.y = divUp(g_h,threads.y);

	cudaMemcpy(temp_left_img,host_left,g_w*sizeof(uchar3)*g_h,cudaMemcpyHostToDevice);
	cudaMemcpy(temp_right_img,host_right,g_w*sizeof(uchar3)*g_h,cudaMemcpyHostToDevice);

	rectify_kernel<<<grid,threads>>>(temp_left_img,temp_right_img,left_img,right_img,left_map,right_map,img_pitch,g_w,g_h);
}

void Rectifier::printRectification(uchar3 *left_img, uchar3* right_img){
	dim3 grid(1,1,1);
	dim3 threads(16,16,1);
	grid.x = divUp(g_w*2,threads.x);
	grid.y = divUp(g_h,threads.y);


	uchar3 *combined_img;
	size_t combined_pitch;
	uchar3 * combined_img_host = (uchar3*)malloc(sizeof(uchar3)*g_w*g_h*2);

	cudaMallocPitch((void**)&combined_img,&combined_pitch,g_w*2*sizeof(uchar3),g_h);
	combined_pitch /= sizeof(uchar3);

	printRectification_kernel<<<grid,threads>>>(left_img,right_img,combined_img,img_pitch,combined_pitch,g_w,g_h);

	cudaMemcpy(combined_img_host,combined_img,g_w*2*sizeof(uchar3)*g_h,cudaMemcpyDeviceToHost);

	FILE* imagefile;
	char Img_name[255];
	Img_name[0]='\0';

	sprintf(Img_name,"combined%d.ppm",2);

	imagefile = fopen(Img_name,"wb");

	fprintf( imagefile, "P6\n%u %u 255\n", g_w*2 , g_h );
	fwrite( (const char *)combined_img_host, 1,
				g_w* g_h*2*3, imagefile);
	cudaFree(combined_img);
	free(combined_img_host);
}

__global__ void printRectification_kernel(uchar3 *left_img, uchar3* right_img, uchar3* combined_img, size_t img_pitch, size_t combined_pitch, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

	if((y < height) && (x < width*2)){
		uchar3 line;
		line.x=255;
		line.y=0;
		line.z=255;
		if( x < width){
			combined_img[__mul24(y,combined_pitch)+x] = left_img[__mul24(y,img_pitch)+x];
		}else{
			combined_img[__mul24(y,combined_pitch)+x] = right_img[__mul24(y,img_pitch)+x%img_pitch];
		}
		if(y%COMBINED_LINE_SPACING == COMBINED_LINE_SPACING/2){
			combined_img[__mul24(y,combined_pitch)+x] = line;
		}
	}
}
//maybe later make the maps textures for faster access
__global__ void rectify_kernel(uchar3* tmp_left, uchar3* tmp_right, uchar3* left, uchar3 *right,float2 *left_map,float2 *right_map, size_t img_pitch, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;
	float2 rec;
	int4 cords;
	float4 tmp_percents,percents;
	uchar3 results;
	uchar3 temp1,temp2,temp3,temp4;

	if((y < height) && (x < width)){
		uchar3 zero;
		zero.x=0;
		zero.y=0;
		zero.z=0;

		//do left image for pixel
		rec = left_map[__mul24(y,img_pitch)+x];

		cords.x = (int)rec.x;
		cords.z=cords.x+1;
		tmp_percents.x = rec.x -cords.x;
		tmp_percents.z = 1-tmp_percents.x;

		cords.y = (int)rec.y;
		cords.w = cords.y+1;
		tmp_percents.y = rec.y -cords.y;
		tmp_percents.w = 1-tmp_percents.y;

		percents.x = tmp_percents.y *tmp_percents.x;
		percents.y = tmp_percents.w *tmp_percents.x;
		percents.z = tmp_percents.y *tmp_percents.z;
		percents.w = tmp_percents.w *tmp_percents.z;

		if(cords.z < width && cords.x >= 0 && cords.w < height && cords.y >= 0){
			temp1=tmp_left[__mul24(cords.y,img_pitch)+cords.x];
			temp2=tmp_left[__mul24(cords.w,img_pitch)+cords.x];
			temp3=tmp_left[__mul24(cords.y,img_pitch)+cords.z];
			temp4=tmp_left[__mul24(cords.w,img_pitch)+cords.z];
			results.x = (unsigned char)(temp1.x *percents.x +temp2.x *percents.y + temp3.x *percents.z +temp4.x*percents.w);
			results.y = (unsigned char)(temp1.y *percents.x +temp2.y *percents.y + temp3.y *percents.z +temp4.y*percents.w);
			results.z = (unsigned char)(temp1.z *percents.x +temp2.z *percents.y + temp3.z *percents.z +temp4.z*percents.w);
			left[__mul24(y,img_pitch)+x] = results;

		}else{
			left[__mul24(y,img_pitch)+x] = zero;
		}

		//do right image for pixel
		rec =  right_map[__mul24(y,img_pitch)+x];

		cords.x = (int)rec.x;
		cords.z=cords.x+1;
		tmp_percents.x = rec.x -cords.x;
		tmp_percents.z = 1-tmp_percents.x;

		cords.y = (int)rec.y;
		cords.w = cords.y+1;
		tmp_percents.y = rec.y -cords.y;
		tmp_percents.w = 1-tmp_percents.y;

		percents.x = tmp_percents.y *tmp_percents.x;
		percents.y = tmp_percents.w *tmp_percents.x;
		percents.z = tmp_percents.y *tmp_percents.z;
		percents.w = tmp_percents.w *tmp_percents.z;

		if(cords.z < width && cords.x >= 0 && cords.w < height && cords.y >= 0){
			temp1=tmp_right[__mul24(cords.y,img_pitch)+cords.x];
			temp2=tmp_right[__mul24(cords.w,img_pitch)+cords.x];
			temp3=tmp_right[__mul24(cords.y,img_pitch)+cords.z];
			temp4=tmp_right[__mul24(cords.w,img_pitch)+cords.z];
			results.x = (unsigned char)(temp1.x *percents.x +temp2.x *percents.y + temp3.x *percents.z +temp4.x*percents.w);
			results.y = (unsigned char)(temp1.y *percents.x +temp2.y *percents.y + temp3.y *percents.z +temp4.y*percents.w);
			results.z = (unsigned char)(temp1.z *percents.x +temp2.z *percents.y + temp3.z *percents.z +temp4.z*percents.w);
			right[__mul24(y,img_pitch)+x] = results;//tmp_right[(int)(__mul24(rec.y,img_pitch)+rec.x)];
		}else{
			zero.x=255;
			zero.y=255;
			zero.z=255;
			right[__mul24(y,img_pitch)+x] = zero;
		}
	}
}


