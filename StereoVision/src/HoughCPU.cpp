/*
 * HoughCPU.cpp
 *
 *  Created on: May 18, 2010
 *      Author: igvcmqp
 */
#include <HoughCPU.h>
#include <iostream>
using namespace::std;

void houghTransform(unsigned char* local_map){
	IplImage* map_img = cvLoadImage("../Images/seg_img1.pgm", 0);
	//map_img->imageData=(char*)local_map;
    IplImage* color_dst = cvCreateImage( cvGetSize(map_img), 8, 3 );
    CvMemStorage* storage = cvCreateMemStorage(0);
    CvSeq* lines = 0;
    int i;

    cvCvtColor( map_img, color_dst, CV_GRAY2BGR );

    lines = cvHoughLines2( map_img, storage, CV_HOUGH_PROBABILISTIC, 1, CV_PI/180, 20, 5, 80 );
    cout<<"total lines:"<<lines->total<<endl;
    for( i = 0; i < lines->total; i++ )
    {
        CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i);
        cout<<"line_start:"<<line[0].x<<","<<line[0].y<<";"<<"line_end:"<<line[1].x<<","<<line[1].y<<endl;
        cvLine( color_dst, line[0], line[1], CV_RGB(150,0,0), 1, 8 );
    }
    int temp;
    for(int i=0;i<101;i++)
    	for(int j=0;j<201;j++){
    		temp = (int)cvGet2D(color_dst, i, j).val[2];
    		if(temp!=0) cout<<"wtf:"<<temp<<endl;
    		if(temp == 150) local_map[i*201+j]=255;
    		else local_map[i*201+j]=0;
    	}
    cvNamedWindow( "Hough", 1 );
    cvShowImage( "Hough", color_dst );

    cvWaitKey(0);
}
