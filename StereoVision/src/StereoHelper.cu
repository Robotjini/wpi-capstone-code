#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <StereoHelper.h>
using namespace std;


int divUp(int a, int b)
{
	if(a%b == 0)
		return a/b;
	else
		return a/b + 1;
}

void saveImage(char *name, int counter, unsigned char * img, int channels, int width, int height){
	FILE* imagefile;
	char path[255];
	path[0]='\0';

	sprintf(path,"../Images/%s%d.pgm",name,counter);
	imagefile = fopen(path,"wb");

	if(channels==1){
		fprintf( imagefile, "P5\n%u %u 255\n", width , height );
		fwrite( (const char *)img, 1,width* height, imagefile);
	}else if (channels==3){
		fprintf( imagefile, "P6\n%u %u 255\n", width , height );
		fwrite( (const char *)img, 1,width* height*3, imagefile);
	}else{
		cout<<"saveImage function only accepts one and three bit image formats ir channel must be 1 or 3"<<endl;
	}

	fclose(imagefile);
}

void print_GPU_mem(){
	// for debug - show the memory on the GPU
	size_t free,total;
	cuMemGetInfo(&free,&total);
	printf("Memory After Allocation - Free: %d, Total: %d\n",free/(1024*1024),total/(1024*1024));
}

void printToFile(char* file_name, int *array, int width, int height){
	ofstream file;
	file.open(file_name);

	for(int i =0;i<height;i++){
		for(int j=0;j<width;j++){
			file<<array[i*width+j];
			if(j<width-1) file<<",";
		}
		file<<endl;
	}
	file.close();
}

void printToFile(char* file_name, float *array, int width, int height){
	ofstream file;
	file.open(file_name);
	for(int i =0;i<height;i++){
		for(int j=0;j<width;j++){
			file<<(float)array[i*width+j];
			if(j<width-1) file<<",";
		}
		file<<endl;
	}
	file.close();
}
void printToFile(char* file_name, float4 *array, int width, int height){
	ofstream file;
	file.open(file_name);
	for(int i =0;i<height;i++){
		for(int j=0;j<width;j++){
			file<<array[i*width+j].x<<","<<array[i*width+j].y<<","<<array[i*width+j].z<<","<<array[i*height+j].w;
			if(j<width-1) file<<",";
		}
		file<<endl;
	}
	file.close();
}

void printToFile(char* file_name, float3 *array, int width, int height){
	ofstream file;
	file.open(file_name);
	for(int i =0;i<height;i++){
		for(int j=0;j<width;j++){
			file<<"["<<array[i*width+j].x<<","<<array[i*width+j].y<<","<<array[i*width+j].z<<"]";
			if(j<width-1) file<<",";
		}
		file<<endl;
	}
	file.close();
}

void printToFile(char* file_name, float2 *array, int width, int height){
	ofstream file;
	file.open(file_name);
	for(int i =0;i<height;i++){
		for(int j=0;j<width;j++){
			file<<"["<<array[i*width+j].x<<","<<array[i*width+j].y<<"]";
			if(j<width-1) file<<",";
		}
		file<<endl;
	}
	file.close();
}

void printToFile(char* file_name, unsigned char *array, int width, int height){
	ofstream file;
	file.open(file_name);

	for(int i =0;i<height;i++){
		for(int j=0;j<width;j++){
			file<<(int)array[i*width+j];
			if(j<width-1) file<<",";
		}
		file<<endl;
	}
	file.close();
}

