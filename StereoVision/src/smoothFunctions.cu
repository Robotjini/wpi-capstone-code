__global__ void K_neighbors(int* cluster_map, unsigned char* grayImage, unsigned char* seg_image_temp, size_t gray_pitch, int width, int height);
__global__ void smooth_clusters(int* cluster_map, unsigned char* grayImage, unsigned char* seg_image_temp, size_t gray_pitch, int width, int height);

__global__ void smooth_clusters(int* cluster_map, unsigned char* grayImage, unsigned char* seg_image_temp, size_t gray_pitch, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

	float count=0;
	float h_ci;
	int radius=1;
	float cluster_val=0;

	if(x < width && y < height){
		for(int i=radius*-1;i<radius*2;i++){
			for(int j=radius*-1;j<radius*2;j++){
				if(y+j < height  && x+i<width){
					get_guassian(&h_ci ,1,x+i,y+j,x,y,radius,radius);
					cluster_val += h_ci * grayImage[__mul24(y+j,gray_pitch)+x+i];
					count += h_ci;
				}
			}
		}
		cluster_val /= count;
		cluster_val = round(cluster_val);
		seg_image_temp[__mul24(y,gray_pitch)+x] = (int)cluster_val;
	}
}

__global__ void K_neighbors(unsigned char* clusterImage,size_t cluster_pitch, int width, int height){
	int x = __mul24(blockIdx.x,blockDim.x) + threadIdx.x;
	int y = __mul24(blockIdx.y,blockDim.y) + threadIdx.y;

	float count=0;
	float h_ci;
	int radius=1;
	int2 kn[9];
	float cluster_val=0;
	int found=0;
	int index=0;

	if(x < width && y < height){
		for(int i=radius*-1;i<radius*2;i++){
			for(int j=radius*-1;j<radius*2;j++){
				cluster_val = clusterImage[__mul24(y+j,cluster_pitch)+x+i];
				found=0;
				for(int z=0;z<9;z++){
					if (cluster_val == kn[z].x){
						kn[z].y++;
						found=1;
						break;
					}
				}
				if (!found){
					kn[index].x = cluster_val;
					kn[index++].y =1;
				}
			}
		}
		int max,max_index;
		max_index=0;
		max = kn[0].y;
		for(int i=0;i<index;i++){
			if (max < kn[i].y){
				max_index=i;
			}
		}
		clusterImage[__mul24(y,cluster_pitch)+x] = (unsigned char)max;
	}
}
