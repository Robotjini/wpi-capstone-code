/*
 * StereoVision.cpp
 *
 *  Created on: May 3, 2010
 *      Author: igvcmqp
 */
#include <StereoVision.h>
Camera* StereoVision::ptGreyCam;
StereoCUDA* StereoVision::stereoVision;
int StereoVision::running;

StereoVision::StereoVision(Cmpt *rvision){
	stereoVision = new StereoCUDA(IMAGE_WIDTH, IMAGE_HEIGHT);
	ptGreyCam = new Camera();
	running=1;
	this->rvision = rvision;
}

StereoVision::~StereoVision(){

}

void StereoVision::runStereoVision(){
	pthread_t thread;
	long rc = pthread_create(&thread,NULL, run, NULL);
	if (rc){
	   printf("ERROR; return code from pthread_create() is %d\n",rc);
	}
}

void* StereoVision::run(void* args){
	// Allocate CPU memory for the point grey camera
	unsigned char* leftImg_host = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*3);
	unsigned char* rightImg_host = (unsigned char *)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*3);
	unsigned char* leftdis = (unsigned char*)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*sizeof(unsigned char));
	unsigned char* segImg = (unsigned char*)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*sizeof(unsigned char));

	ptGreyCam->initialize(IMAGE_WIDTH,IMAGE_HEIGHT);
	while(running){
		ptGreyCam->capture();
		leftImg_host[0]='\0';
		rightImg_host[0]='\0';
		ptGreyCam->download(leftImg_host,rightImg_host);

		if(leftImg_host==NULL || rightImg_host ==NULL) cout<<"Images are null...."<<endl;
		stereoVision->stereoProcess(leftImg_host, rightImg_host,leftdis,segImg);
	}
	ptGreyCam->~Camera();
	stereoVision->~StereoCUDA();
}

void StereoVision::kill(){
	running=0;
}
