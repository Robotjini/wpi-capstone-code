#ifndef __POINTTGREYCAMERA_H__
#define __POINTGREYCAMERA_H__

// Point Grey Control Class
// Originally developed by David Gallup at UNC

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ctype.h>
using namespace std;

#include </usr/local/include/opencv/cv.h>
#include </usr/local/include/libraw1394/raw1394.h>
#include </usr/local/include/dc1394/dc1394.h>
#include </usr/local/include/opencv/highgui.h>
#include </usr/local/include/opencv/cxmisc.h>

#define _1394b	1
#define CAMERA_BUFFER	8
#define IMAGE_LIST	"stereo_calib.txt"//name of calibration image text file
#define SQUARE_SIZE	.04445	//(meters)actual size of the chessboard squares in the chessboard used to calibrate.


class PtGreyCamera {
public:
	PtGreyCamera();
	~PtGreyCamera();
	void init( int w, int h);
	void SetColorBuffers(unsigned char * left, unsigned char * right);
	void clean();

	int imageWidth() const;
	int imageHeight() const;
	int imageBPP() const;
	CvMat* leftCameraIntrinsic(){return &_M1;}
	CvMat* rightCameraIntrinsic(){return &_M2;}
	CvMat* rotationMatrix(){return &_R;}
	CvMat* translationMatrix(){return &_T;}
	void calibrate(int debug);
	void capture();
	void printMatrix(CvMat *M, int rows, int cols , char *name );
	void clean_and_exit(dc1394error_t err, string msg);
	void download(unsigned char * left, unsigned char * right );
	unsigned char *leftImage() { return leftframe->image; }
	unsigned char *rightImage() { return rightframe->image; }
	bool hasColor() { return true; }
	unsigned char *leftColorImage() { return leftframe->image; }
	unsigned char *rightColorImage() { return rightframe->image; }
	double R[3][3], T[3], E[3][3], F[3][3], R1[3][3], T1[3], R2[3][3], T2[3];

private:
	dc1394camera_t* cameras[2];
	dc1394video_frame_t *leftframe;
	dc1394video_frame_t *rightframe;
	dc1394_t *dc_type;
	CvMat _M1;//left camera intrinsic parameters
	CvMat _M2;//right camera intrinsic parameters
	CvMat _D1;//left camera distortion parameters
	CvMat _D2;//right camera distortion parameters
	CvMat _R;//rotation matrix in relation to ??? camera
	CvMat _T;//translation matrix in relation to ??? camera
	CvMat _R1;
	CvMat _T1;
	CvMat _R2;
	CvMat _T2;
	double M1[3][3], M2[3][3], D1[5], D2[5];
	int height, width;
};

#define DIGERROR(ee) \
		{ \
	DigiclopsError e = ee; \
	if (e) { \
		printf("DigiclopsError: %s at %s %d\n", \
				digiclopsErrorToString(e),__FILE__,__LINE__); \
				exit(13); \
	} \
		}

#define TRIERROR(ee) \
		{ \
	TriclopsError e = ee; \
	if (e) { \
		printf("TriclopsError: %s at %s %d\n", \
				triclopsErrorToString(e),__FILE__,__LINE__); \
				exit(13); \
	} \
		}


#endif
