/*
 * HoughCPU.h
 *
 *  Created on: May 18, 2010
 *      Author: igvcmqp
 */

#ifndef HOUGHCPU_H_
#define HOUGHCPU_H_
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxmisc.h>

void houghTransform(unsigned char* local_map);

#endif /* HOUGHCPU_H_ */
