/*
 * rectify.h
 *
 *  Created on: Mar 26, 2010
 *      Author: igvcmqp
 */

#ifndef RECTIFY_H_
#define RECTIFY_H_
#include </usr/local/cuda/include/driver_types.h>
#include </usr/local/cuda/include/cuda.h>
#include </usr/local/cuda/include/cuda_runtime.h>
#include </home/cgamache/NVIDIA_GPU_Computing_SDK/C/common/inc/cutil.h>

#define MX_LEFT		"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/mx_left.txt"
#define MY_LEFT		"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/my_left.txt"
#define MX_RIGHT	"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/mx_right.txt"
#define MY_RIGHT	"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/my_right.txt"

class Rectifier{
	public:
		Rectifier(unsigned int width, unsigned int height);
		~Rectifier();
		void rectify(uchar3 *host_left, uchar3* host_right, uchar3 *left_img, uchar3 *right_img);
		void printRectification(uchar3 *left_img, uchar3* right_img);
	private:
		void loadMaps();
		int g_w,g_h;
		float2 *left_map;//map connecting left pixel points to rectified pixel points
		float2 *right_map; //map connecting right pixel points to rectified pixel points
		uchar3 *temp_left_img;
		uchar3 *temp_right_img;
		size_t img_pitch;
};

__global__ void rectify_kernel(uchar3* tmp_left, uchar3* tmp_right, uchar3* left, uchar3 *right,float2 *left_map,float2 *right_map, size_t img_pitch, int width, int height);
__global__ void printRectification_kernel(uchar3 *left_img, uchar3* right_img, uchar3* combined_img, size_t img_pitch, size_t combined_pitch, int width, int height);

#endif /* RECTIFY_H_ */
