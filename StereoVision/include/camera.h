/*
 * camera.h
 *
 *  Created on: Mar 22, 2010
 *      Author: igvcmqp
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include <libraw1394/raw1394.h>
#include <dc1394/dc1394.h>
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include <iostream>
#include <ctype.h>
//#include <Magick++.h>

using namespace::std;


#define _1394b	1
#define CAMERA_BUFFER	4
#define IMAGE_LIST	"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/stereo_calib.txt"//name of calibration image text file
#define SQUARE_SIZE	.04445	//(meters)actual size of the chessboard squares in the chessboard used to calibrate.

class Camera
{
public:
	Camera();
	~Camera();
	void initialize(int width,int height);
	void clean_and_exit(dc1394error_t err,string msg);
	void clean();
	int getImageWidth();
	int getImageHeight();
	void capture();
	void download(unsigned char * left, unsigned char * right );
	void downloadLeft(unsigned char *left);
	void getFrameAndResize(unsigned char* imageBuffer);

private:
	dc1394camera_t* cameras[2];
	dc1394video_frame_t *leftframe;
	dc1394video_frame_t *rightframe;
	dc1394_t *dc_type;
	int height, width;
	dc1394camera_list_t *list;
	pthread_mutex_t captureMutex;
};

#endif /* CAMERA_H_ */
