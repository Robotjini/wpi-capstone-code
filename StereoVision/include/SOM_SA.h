
#ifndef SOM_SA_H_
#define SOM_SA_H_
#include </usr/local/cuda/include/driver_types.h>
#include </usr/local/cuda/include/cuda.h>
#include </usr/local/cuda/include/cuda_runtime.h>
#include </home/cgamache/NVIDIA_GPU_Computing_SDK/C/common/inc/cutil.h>

#define SOM_MAP_SIZE	16
#define NUM_CLUSTERS	7
#define LEARN_RATE2		.0005
#define RADIUS2			2
#define SOM_FILENAME "/home/cgamache/Stereo vision/SOM.txt"

class SOM_SA{
	public:
		SOM_SA(int width,int height);
		~SOM_SA();
		void segment(float3 *LuvImage, size_t Luv_pitch, unsigned char **seg_image);
		__host__ void create_SOM_paper_test(float3 *LuvImage);
		__host__ void create_SOM(float3 *LuvImage);
		void train(float3 *LuvImage);
		void loadSOM(char *file_name);
		void cluster_SOM();
		void print(int counter);
		void segmentBasedAverage(float *disp_map, unsigned char **return_seg_img);
		void convertToGreyScale(float3 *LuvImage_left, float3* LuvImage_right, size_t Luv_pitch, unsigned char *dest_left, unsigned char *dest_right);
	private:
		void train_SOM(float4 *SOM_map, float* ssd, float3* LuvImage, size_t SOM_pitch, size_t ssd_pitch, float radius_init, float learn_rate_init);
		__host__ int get_min_ssd(float *ssd);
		__host__ void initialize_SOM(float4 *random_array);
		float4 * SOM_map;
		float * ssd;
		unsigned char *clusterImage;
		size_t SOM_pitch;
		size_t ssd_pitch;
		size_t cluster_pitch;
		int g_w,g_h;
		unsigned char* segImage;
		//texture<float4, 2, cudaReadModeNormalizedFloat> SOM_Tex;
};
__global__ void segmentBasedAverage_kernel(float *disp_map, unsigned char *seg_image, size_t img_step, int width, int height);
__global__ void convertToGreyScale_kernel(unsigned char *destImage,float3* LuvImage, float4* SOM_map, size_t Gray_pitch, size_t Luv_pitch, size_t SOM_pitch, int width, int height);
__global__ void initialize_clusters(float4 *SOM_map,float *rand);
__global__ void segment_image(unsigned char* clusterImage, unsigned char* seg_image, size_t cluster_pitch, int width, int height);
__global__ void adjust_nodes(float4 *rand, float4 *SOM_map, float3 *clusters, size_t SOM_pitch);
__global__ void compute_cluster_centers(float4* SOM_map, float3* clusters);
__global__ void compute_energy(float* return_energy, float *energy, float4* SOM_map, float3* clusters);
__device__ void get_guassian(float *h_ci ,float A, float x, float y, float h, float k, float spread_x, float spread_y);
__global__ void calculate_ssd(float4 * SOM_map, float *ssd, float3 *input_luv_pixel, size_t SOM_pitch, size_t ssd_pitch);
__global__ void update_weights(float4 *SOM_map, float3 *Luv_pixel, size_t SOM_pitch, float4 *xyrlr_dev);
__global__ void SOM_colorReduction(unsigned char *GrayImage,float3* LuvImage, float4* SOM_map, size_t Gray_pitch, size_t Luv_pitch, size_t SOM_pitch, int width, int height);
__global__ void K_neighbors(unsigned char* clusterImage,unsigned char* segImage, size_t cluster_pitch, int width, int height);

#endif
