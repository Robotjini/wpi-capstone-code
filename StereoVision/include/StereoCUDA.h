/*
 * SOM_SA.h
 *
 *  Created on: Feb 25, 2010
 *      Author: igvcmqp
 */

#ifndef STEREOCUDA_H_
#define STEREOCUDA_H_
#include <GL/glew.h>

#include </usr/local/cuda/include/driver_types.h>
#include </usr/local/cuda/include/cuda.h>
#include </usr/local/cuda/include/cuda_runtime.h>
#include </home/cgamache/NVIDIA_GPU_Computing_SDK/C/common/inc/cutil.h>
#include </usr/local/cuda/include/cudaGL.h>

#include </usr/local/cuda/include/cuda_gl_interop.h>
#include <StereoHelper.h>
#include <rectify.h>
#include <SOM_SA.h>
#include <camera.h>
#include <LineDetector.h>
//#include <StereoMapper.h>

//Defined constants:
#define ROWSperTHREAD 10   // the number of rows a thread will process
#define BLOCK_W 128			 // the thread block width
#define RADIUS_H 2	 // Kernel Radius 5V & 5H = 11x11 kernel
#define RADIUS_V 2
#define MIN_SSD 150   // The mimium acceptable SSD value
#define STEREO_MIND -128.0f   // The minimum d range to check
#define STEREO_MAXD 128.0f	 // the maximum d range to check
#define STEREO_DISP_STEP 1.0f // the d step, must be <= 1 to avoid aliasing
#define SHARED_MEM_SIZE ((BLOCK_W + 2*RADIUS_H)*sizeof(int) ) // amount of shared memory used


class StereoCUDA{
	public:
		StereoCUDA(unsigned int width, unsigned int height);
		~StereoCUDA();
		float stereoProcess(unsigned char * p_hostLeft,unsigned char * p_hostRight, unsigned char *leftdis, unsigned char* seg_image);
		void UnregisterGLBufferForCUDA(int buffer);
		void* run_main_loop(void *args);
		void SetDisplayImageBuffers (unsigned int Left, unsigned int Right,unsigned int Disparity,unsigned int Disparity2);
	private:
		//The Image width & height.
		int g_w;
		int g_h;

		float3 * LuvImageLeft;
		float3 * LuvImageRight;
		uchar3 *LeftImage;
		uchar3 *RightImage;
		size_t Luv_pitch;
		SOM_SA *SOM_MAP;
	//	StereoMapper *mapper;
		Rectifier *rectifier;
		LineDetector *lineDetector;
		Camera ptGreyCam;
		size_t RGB_pitch;
		int SOM_TRAINED;

		//Pointers to memory for the disparity value (d) and the current minimum SSD, also on the GPU:
		float *g_disparityLeft;
		float *g_disparityLeft2;
		int *g_minSSD;
		size_t g_floatDispPitch;

		//space for images in host space
		unsigned char * leftImg_host;
		unsigned char * rightImg_host;

		//These values store OpenGL buffer ID�s which are used to draw the image on the screen using CUDA�s OpenGL interop capability.  These are set from the main program after the bufferes are created..
		unsigned int LeftImage_GLBufferID;
		unsigned int RightImage_GLBufferID;
		unsigned int DisparityImage_GLBufferID;
		unsigned int DisparityImage2_GLBufferID;

		//Pointers to cudaArrays, which contain a copy of the image data for texturing
		cudaArray * g_leftTex_array;
		cudaArray * g_rightTex_array;
};

__global__ void convertRGB_to_MLUV(uchar3 *src, float3 * dest, size_t src_pitch, size_t dest_pitch, int width, int height);
__global__ void drawDisparityKernel(	uchar4 * out_image, size_t out_pitch, float *disparity, size_t disparity_pitch, int width, int height);

#endif /* STEREO_H_ */
