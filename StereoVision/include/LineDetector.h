/*
 * LineDetector.h
 *
 *  Created on: Apr 14, 2010
 *      Author: igvcmqp
 */

#ifndef LINEDETECTOR_H_
#define LINEDETECTOR_H_
#include </usr/local/cuda/include/driver_types.h>
#include </usr/local/cuda/include/cuda.h>
#include </usr/local/cuda/include/cuda_runtime.h>
#include </home/cgamache/NVIDIA_GPU_Computing_SDK/C/common/inc/cutil.h>
#include <StereoHelper.h>

#define IMAGE_SENSOR_WIDTH		.48 //1/3ccd sensor width in centimeters
#define IMAGE_SENSOR_HEIGHT		.36 // 1/3 ccd sensor height in centimeters
#define FOCAL_LENGTH			.48	//camera lens focal length in centimeters

class LineDetector{
	public:
		LineDetector(int width, int height);
		~LineDetector();
		void segmentLines(float3* LuvImage, unsigned char** line_img);
		void drawBlocks(uchar3 *blocks_img);
		void detectLines(unsigned char *seg_img, unsigned char **ret_img);
		void detectCones(unsigned char *seg_img);
	private:
		void initialize();
		float loadVerticalAOV();
		float loadHorizontalAOV();
		unsigned char *line_segments;
		int g_w, g_h;
		size_t img_pitch;
		int4* block_limits;
		unsigned char* local_map;
		size_t map_pitch;
};

__global__ void cleanCones_kernel(unsigned char* local_map, size_t map_pitch);
__global__ void detectCones_kernel(unsigned char *seg_img, int4 *block_limits, unsigned char* local_map, size_t img_pitch, size_t map_pitch);
__global__ void detectLines_kernel(unsigned char *seg_img, int4 *block_limits, unsigned char *local_map, size_t img_pitch, size_t map_pitch);
__global__ void drawBlocks_kernel(uchar3 *blocks_img, int4* block_limits, size_t img_pitch, size_t map_pitch);
__device__ void get_color(uchar3* color, int num);
__global__ void segmentLines_kernel(float3* LuvImage, unsigned char* line_segments, size_t img_pitch, int width, int height);
__global__ void initialize_kernel(int4* block_limits, unsigned char* local_map, size_t map_pitch, float horz_AOV, int img_width, int img_height);
__device__ int get_pixelX(float x_dist);
__device__ int get_pixelY(float y_dist, float w_m, int img_width);

#endif /* LINEDETECTOR_H_ */
