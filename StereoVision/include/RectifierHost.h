#ifndef RECTIFYHOST_H_
#define RECTIFYHOST_H_
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxmisc.h>

#define MX_LEFT		"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/mx_left.txt"
#define MY_LEFT		"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/my_left.txt"
#define MX_RIGHT	"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/mx_right.txt"
#define MY_RIGHT	"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/my_right.txt"

class RectifierHost{
	public:
		RectifierHost(unsigned int width, unsigned int height);
		~RectifierHost();
		void rectify(unsigned char *host_left, unsigned char* host_right);
	private:
		void loadMaps();
		int g_w,g_h;
		float *mx_left, *mx_right, *my_left, *my_right;
		CvMat _mx_left, _mx_right, _my_left, _my_right;
};
#endif /* RECTIFYHOST_H_ */
