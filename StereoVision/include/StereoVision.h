/*
 * StereoVision.h
 *
 *  Created on: May 3, 2010
 *      Author: igvcmqp
 */

#ifndef STEREOVISION_H_
#define STEREOVISION_H_
#include <camera.h>
#include <StereoCUDA.h>
#include <svSensor.h>
#define IMAGE_WIDTH		1024
#define IMAGE_HEIGHT	768

class StereoVision{
	public:
		StereoVision();
		~StereoVision();
		void runStereoVision();
		void kill();
	private:
		static void* run(void *args);
		static StereoCUDA *stereoVision;
		static Camera *ptGreyCam;
		static int running;
		static Cmpt *rvision;
};

#endif /* STEREOVISION_H_ */
