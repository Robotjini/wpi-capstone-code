/*
 * StereoMapper.h
 *
 *  Created on: Apr 3, 2010
 *      Author: igvcmqp
 */

#ifndef STEREOMAPPER_H_
#define STEREOMAPPER_H_

#define TRANSLATION_MATRIX		"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/T.txt"
#define PIXEL_SIZE				.000465f //in centimeters
#define IMAGE_SENSOR_WIDTH		.48 //1/3ccd sensor width in centimeters
#define IMAGE_SENSOR_HEIGHT		.36 // 1/3 ccd sensor height in centimeters
#define FOCAL_LENGTH			.48	//camera lens focal length in centimeters

#include </usr/local/cuda/include/driver_types.h>
#include </usr/local/cuda/include/cuda.h>
#include </usr/local/cuda/include/cuda_runtime.h>
#include </home/cgamache/NVIDIA_GPU_Computing_SDK/C/common/inc/cutil.h>

class StereoMapper{
	public:
		StereoMapper(int img_w,int img_h);
		~StereoMapper();
		void map2D(int *dest, float *disp_map, size_t img_pitch, int img_width, int img_height);
	private:
		float loadBaseline();
		float loadFocalLength();
		float loadHorizontalAOV();
		float loadVerticalAOV();

		int map_w, map_h;
		int *local_map;
		float3 *xy_map;
		float focal_len; //focal length of the cameras represented in pixels
		float baseline; //distance between the two cameras(in pixels) taken as the magnitude of the translation matrix
		size_t map_pitch;
		float horizontal_AOV, vertical_AOV;
};

__global__ void clean_local_map(int* local_map, size_t map_pitch);
__global__ void pop_local_map(float3 *xy_map, int* local_map, size_t img_pitch, size_t map_pitch, int img_width, int img_height);
__global__ void init_local_map(int* local_map, size_t map_pitch);
__global__ void map2D_kernel(float *disp_map, float3 *xy_map, size_t img_pitch, float baseline, float focal_len, float horz_AOV, float vert_AOV, int img_width, int img_height);

#endif /* STEREOMAPPER_H_ */
