/*
 * StereoDebug.h
 *
 *  Created on: Mar 15, 2010
 *      Author: igvcmqp
 */

#ifndef STEREODEBUG_H_
#define STEREODEBUG_H_
#include </usr/local/cuda/include/driver_types.h>
#include </usr/local/cuda/include/cuda.h>
#include </usr/local/cuda/include/cuda_runtime.h>
#include </home/cgamache/NVIDIA_GPU_Computing_SDK/C/common/inc/cutil.h>

void printToFile(char* file_name, int *array, int width, int height);
void printToFile(char* file_name, unsigned char *array, int width, int height);
void printToFile(char* file_name, float4 *array, int width, int height);
void printToFile(char* file_name, float *array, int width, int height);
void printToFile(char* file_name, float2 *array, int width, int height);
void printToFile(char* file_name, float3 *array, int width, int height);
void saveImage(char *name, int counter, unsigned char * img, int channels, int width, int height);
void print_GPU_mem();
int divUp(int a, int b);

#endif /* STEREODEBUG_H_ */
