/*
 * calibration.h
 *
 *  Created on: Mar 23, 2010
 *      Author: igvcmqp
 */

#ifndef CALIBRATION_H_
#define CALIBRATION_H_

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxmisc.h>


//#define CAMERA_BUFFER	8
#define IMAGE_LIST	"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/stereo_calib.txt"//name of calibration image text file
#define MX_LEFT		"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/mx_left.txt"
#define MY_LEFT		"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/my_left.txt"
#define MX_RIGHT	"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/mx_right.txt"
#define MY_RIGHT	"/home/igvcmqp/workspace/prometheus/src/applications/ojVehicleSim/config/my_right.txt"
#define SQUARE_SIZE	.073025	//(meters)actual size of the chessboard squares in the chessboard used to calibrate.

class CameraCalibration
{
public:
	CameraCalibration();
	~CameraCalibration();
	CvMat* leftCameraIntrinsic(){return &_M1;}
	CvMat* rightCameraIntrinsic(){return &_M2;}
	CvMat* rotationMatrix(){return &_R;}
	CvMat* translationMatrix(){return &_T;}
	void calibrate(int debug);
	void printMatrix(CvMat *M, int rows, int cols ,char *name );
private:
	double R[3][3], T[3], E[3][3], F[3][3], R1[3][3], T1[3], R2[3][3], T2[3];
	CvMat _M1;//left camera intrinsic parameters
	CvMat _M2;//right camera intrinsic parameters
	CvMat _D1;//left camera distortion parameters
	CvMat _D2;//right camera distortion parameters
	CvMat _R;//rotation matrix in relation to ??? camera
	CvMat _T;//translation matrix in relation to ??? camera
	CvMat _R1;
	CvMat _T1;
	CvMat _R2;
	CvMat _T2;
	double M1[3][3], M2[3][3], D1[5], D2[5];


};
void testHough(char *file_name);
void getTestImage(char *file_name, char* imagebuffer,int img_w, int img_h);
#endif /* CALIBRATION_H_ */
